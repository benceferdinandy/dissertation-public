This chapter will present our study titled "Collective motion of groups of self-propelled particles following interactive leaders" \autocite{ferdinandy2017horses} as an example of agent-based modelling. The study concerns itself with the modelling of the collective motion of hierarchical herds, that is herds that are made up from smaller groups of animals. In the next section we will overview the background literature needed to understand the different aspects of the problem and then present our specific motivation in creating the model. The rest of the sections of the chapter will present our work on the problem: the agent-based model we created, the results we obtained, and its discussion.


\section{Background literature}
\label{sec:collmot}
\subsection{Collective motion}
Collective motion is the emergence of ordered motion in a system made up of many autonomous agents. Persistent motion is one of the hallmarks of animal life, yet recently several physical and chemical systems have been found which have self-propelled, interacting units. There are many questions we can ask about these systems. Are the observed phenomena unique to the particular systems, or are there some more general laws that these systems obey? Are these the same for physical, chemical and biological systems? How can we reproduce these motions? Can we build robots that exhibit collective motion or alter the behaviour of existing collective motion? Answering these questions promises advances in fields from animal husbandry to exploration. Collective motion is a subset of more general phenomena called collective behaviour, that has implications of the organisation of society, and as such, is of much interest.


\subsection{The language of collective motion}

The fundamental element of a collective motion system is the self-propelled particle (SPP) \autocite{spporig}.
This is a point-like particle, that can change its velocity at will and has some internal energy reserve that allows for its movement. Since the SPP is a readily identifiable agent it is very straightforward to apply agent-based modelling to such a system and is indeed the more successful method, although there are works that go for continuous media approaches \autocite{csopintro2}. When modelling such systems, instead of physical forces different type of effective forces are taken into account, that often do not even have a force dimension. A system made up of such particles obviously will not conserve energy, momentum or angular momentum. Although an SPP model itself violates very basic physical laws, the systems they are modelling do not. Within a certain range of accelerations the model can be thought of as abstracting away from a unit's internal energy resources that it can use to propel itself and abstracting away from a unit transferring momentum and angular momentum to its environment during movement. Such as it is, most models do not take into account that very abrupt changes are simply not permitted by physical limitations even with the internal energy and the surrounding environment taken into account, but this is thought of as loosing a bit of realism for greatly simplified models. Although details of the actual environment is usually dropped, it is brought back through the introduction of some "thermal" noise acting on the particles.

Although SPP models are inherently non-equilibrium, they show some marked parallels with equilibrium statistical physics. In statistical physics the renormalization group method has shown that when a system undergoes a continuous phase transition, the particular details of the system in question become irrelevant and only a few relevant parameters remain. A system will have an order parameter, which measures the transition from one phase to the other and changes continuously, while the relevant parameters obey scaling laws at the point of transition. The values of the critical exponents with which these parameters scale allow for the classification of the different systems into universality classes. Remarkably, a very similar thing happens in SPP systems and both the order parameter and the scaling can be defined, implying that phase transitions in very different SPP systems should be very similar, as in equilibrium statistical physics \autocite{Vicsek2012}.



\subsection{Evidence}

Collective motion has been observed from physical systems through cells to humans, but to quantitatively analyse and back up models with data each unit has to be tracked in space. This is a non-trivial task, since usually one must observe many units (up to the thousands in say starling flocks), which move rather fast in an open space and look very similar, although for example in bacteria, the space is confined and the observables slow, but on the other hand they are rather tiny.

Several techniques have been developed to record collective movement each suited for different scenarios. For cells, the particle image velocimetry method has been adapted (the method originally uses tracer particles added to a continuous media to track the velocity vector field of that media, based on the displacement of the particles). Bacteria have been tracked by using phase contrast microscopy to visualize them.

For larger animals, the unconfined space poses problems. For two dimensional movements, e.g. grazing herds aerial photography, or more recently flying drone based cameras were used to record the animal trajectories. In case of fish, which naturally move in three dimensions it is possible to build an aquarium that essentially forces them to move in two dimensions, making video tracking relatively easy. For three dimensional movement a single camera becomes rather problematic due to overlaps of individuals and in general the missing distance information. Fish are more easily confined compared to birds, allowing for methods not viably for the latter. An early method for reconstructing the three dimensional movement of schools of fish was the shadow method, which used a grided background and the shadows of the fish on this background to determine positions of all the individuals. Later three orthogonally mounted video cameras were used to capture the trajectories of a school of fish.

For birds, such techniques are not possible, since most birds fly over much larger areas than is feasible to confine. To measure nearest neighbour distances in starlings stereoimaging techniques were used, although this did not make it possible to recover individual trajectories. The newest method, made possible by advances in the miniaturization of GPS and other animal-borne sensor technologies: each individual in a flock is supplied with a GPS receiver, which logs trajectories, although there are two shortcomings, first, the errors of the GPS signals are still quite large compared to bird-to-bird distances, especially in the vertical direction and that the time and resources needed to monitor a flock grows almost linearly with the size of the flock \autocite{Vicsek2012}.


\subsection{Simple two dimensional model of collective motion}

The first quantitative treatise of collective motion was the Standard Vicsek Model (SVM), which first presented an SPP model described as a system would be in statistical physics \autocite{spporig}. The SVM describes a very simple system, that allows for two dimensional coherent motion to emerge from an initially disordered state and also shows phase transitions in two relevant parameters.

The system consists of $N$ particles in a periodically bounded box of length $L$. The speed of the particles is constant and the same for all particles. The direction of the velocity is determined by the average velocity of other particles in their neighbourhood of radius $r$ and a random noise. The velocities $\bm{v}_i $ and the positions $ \bm{x}_i $ of the particles are updated simultaneously with the following equation
\begin{equation} 
\bm{x}_i (t+\Delta t)= \bm{x_i}(t) + \bm{v}_i(t)\Delta t
\end{equation}
where the speed of the velocity $\bm{v}_i(t+\Delta t)$ is denoted by  $v$ and its direction is given by the formula
\begin{equation}
\vartheta_i (t+\Delta t) = \langle\vartheta(t)\rangle_r + \Delta\vartheta,
\end{equation}
where $\langle\vartheta(t)\rangle_r$ is the average velocity of the particles within a radius $r$ around the $i$\textsuperscript{th} particle and  $\Delta\vartheta$ is a random noise drawn from the uniform distribution on the interval $[-\eta/2;\eta/2]$. Thus for a given $L$ there are three independent parameters: $\eta$, $v$ and the density  $\rho = N/L^2$. The irrelevant parameters, which can be changed without the model exhibiting much change were set to fix values of $v= 0.03$, $\Delta t = 1$ and $r=1$, and the initial condition was the random uniform distribution of particles in space and also the random uniform distribution of $\vartheta_i$-s.


\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{spp-vicsek.PNG}

\caption[SVM trajectories]{Trajectory segments of the SVM model for different values of $\rho$ and $\eta$. (a) initial condition, (b) small $\rho$ and $\eta$, (c) large $\rho$ and $\eta$, (d) large $\rho$ and small $\eta$ (figure from \autocite{spporig}).
}%
\label{fig:sppvicsek}
\end{figure}

The model shows a continuous phase transition in $\rho$ and $\eta$. The possible states are shown on \cref{fig:sppvicsek}, where 20 steps of each particle's trajectory is plotted. We can see on  a)  the initial conditions, on b) that with low density and noise the particles form small groups, on c) that with large noise and density the particles basically move randomly, finally on d) that with large density and low noise all the particles go in the same direction.

The quantity
\begin{equation}
v_a = \frac{1}{Nv}\left\lvert\sum_j\bm{v}_j\right\rvert
\end{equation}
serves as the order parameter since it is 0 when all particles are moving randomly, and is 1 when all particles are moving in the same direction. With this the critical exponents of the phase transition at  $L \longrightarrow \infty $ can be calculated given
\begin{equation} \label{eq:vicsek-order}
v_a \sim (\eta_c(\rho)-\eta)^{\beta}\mbox{ and } 
v_a \sim (\rho-\rho_c(\eta))^{\delta},
\end{equation}
where $\rho_c(\eta)$ and $\eta_c(\rho)$ is the critical density and noise respectively, as $\beta = 0.45\pm0.07$ and $\delta = 0.35\pm0.06$.

As we can see, noise plays an important role in the SPP system, in that it can induce a phase transition from a disordered to an ordered state, in analogy with the temperature of equilibrium statistical physics. Moreover, in slightly more complicated systems noise can play an even more important part, as a certain level of noise can be a stabilizing factor for some system states. For example in \autocite{Strombom2011} they found that adding attraction to the SPP model there are various motion patterns a group of SPP-s can produce, but some of these patterns are only stable when there is some noise in the system. 

\subsection{Collective motion of tissue cells}
\label{subsec:szabo2006}
\begin{figure}[ht]
\begin{center}
\includegraphics[width=\textwidth]{chaptercsopcsop/cellscollmot.png}
\end{center}
\caption[Collective motion of cells]{
Phase contrast images showing the typical behaviour of cells for three different densities. (a) 1.8, (b) 5.3, (c) 14.7 cells/100x100 μm 2 . Observe that as cell density increases cell motility undergoes collective ordering. The speed of single cells is higher than that of
cells moving in coherent groups. Scale bar: 200 μm. (d)-(f): Velocity of cells. Scale bar: 50 μm/min. Image and caption from \autocite{szabo2006}.}
\label{fig:csopcsop-cellscollmot}
\end{figure}

The study of Szabó et al. \autocite{szabo2006} is briefly introduced for two reasons, first, it is an example of trying to explicitly model an observed system and second, because it had a direct influence on our study. On \autoref{fig:csopcsop-cellscollmot} we can see the observed system of tissue cells and their velocities. As predicted by the SVM, these cells undergo a phase transition from disordered movement to ordered collective movement as the density of the cells is increased.

Although the SVM predicts the behaviour the SVM's explicit averaging of the velocities of neighbouring cells would not be realistic, since cells very probably do not have receptors that would be able supply them with the necessary information to do the calculation. Instead, the collective motion must arise from the pairwise forces acting between the cells, thus in this model of cells each agent tries to align itself with the net forces acting on it.

The movement of the $\bm{r}_i$ cell is described by the overdamped dynamics:
\begin{equation}
\diff{\bm{r}_i(t)}{t} = v_0\bm{n}_i(\theta_i(t)) + 
\sum\limits_{j=1}^N\bm{F}(\bm{r}_i,\bm{r}_j),
\end{equation}
thus each cell is trying to keep a self-propelled velocity ($\bm{n}$) of their own, while neighbouring cells exert forces on them. The self-propelled velocity slowly relaxes to the actual velocity thus:
\begin{equation}
\diff{\theta_i^{\bm{n}}(t)}{t} = \frac{1}{\tau}\arcsin\left( \left[ \bm{n}_i(t) \times \frac{\bm{v}_i(t)}{|\bm{v}_i(t)|}\right] \cdot \bm{e}_z  \right) + \xi,
\end{equation}
where $\xi$ is a noise term and $\bm{e}_z$ is a unit vector perpendicular to the plane of motion.

The pairwise forces are the most simple linear attractive and repulsive forces between defined by an equilibrium distance $R_\text{eq.}$ and a maximum interaction distance~$R_0$, while changing accordingly to the $d_{ij}$ distances between cells:
\begin{equation}
\bm{F}(\bm{r}_i,\bm{r}_j) = \bm{e}_{ij} \times 
\begin{cases}
F_\text{rep.} \frac{d_{ij}- R_\text{eq.}}{R_text{eq.}} & d_{ij} < R_\text{eq.}, \\
F_\text{adh.} \frac{d_{ij}-R_\text{eq.}}{R_0 - R_\text{eq.}} & R_\text{eq.} \leq d_{ij} \leq R_0, \\
0 & R_0 < d_{ij}. \\
\end{cases}
\end{equation}

The critical behaviour of this system is very close to the SVM with $\beta = 0.44 \pm 0.08$ and $\delta = 0.38 \pm 0.07$ (cf. \cref{eq:vicsek-order}). This model adequately reproduces the observed behaviour of the cells, and also shows, that the explicit averaging of the SVM has very similar consequences as the gradual alignment to pairwise attraction and repulsion forces.

\subsection{Coordinated stopping}

In our previous study which is not part of the dissertation we have studied how a bias in the noise felt by the individual can enable coordinated stopping of SPP-s moving in a semi-2D system \autocite{Ferdinandy2012}. As we will later see, among our plans to continue research on the topic of this chapter is the stopping and starting of collective movement in herds, so we will briefly revisit our results on how the noise bias can be used to induce stopping.

The system is semi-2D in the sense that while it models the landing of birds, the movement in the horizontal plane is completely independent of vertical movements (bar actual landing), thus the landing can easily be interpreted as a stopping of some 2D motion.

The mechanism for the vertical motion (and subsequently, the landing) is as follows. The vertical position of a bird is updated as follows

\begin{equation}
z_i(t+\Delta t)=z_{i}(t)+v\frac{f_{\textrm{z},i}^{\textrm{sum}}}{|f_{\textrm{z},i}^{\textrm{sum}}|}\Delta t.
\end{equation}

where the force $f_{\textrm{z},i}^\textrm{sum}$ is the sum of the following parts: $f_{\textrm{z},i}^\textrm{a}$ averaging over neighbouring birds' vertical velocities, $f_{\textrm{z},i}^\textrm{r}$ repulsive force representing the empty space each animal keeps around itself, $f_{\textrm{z},i}^\textrm{h}$ boundary which keeps the birds at a specified height and an $f_{\textrm{z},i}^\textrm{n}$ noise.

%\begin{equation}
%f_{\textrm{z},i}^\textrm{sum} = f_{\textrm{z},i}^\textrm{a}+f_{\textrm{z},i}^\textrm{n}+f_{\textrm{z},i}^\textrm{h} + f_{\textrm{z},i}^\textrm{r}
%\end{equation}
\begin{figure}
\begin{center}
\subfloat[]{\includegraphics[clip,width=0.5\textwidth]{chaptercsopcsop/fig1a.pdf}\label{fig:fig1sub}}
\subfloat[]{\includegraphics[clip,width=0.5\textwidth]{chaptercsopcsop/fig1b.pdf}\label{fig:fig2sub}}
\end{center}
\caption[Landing forces overview]{(a) The force $f_\textrm{z}^\textrm{h}$ against $z$. The plot shows the small forceless regime around $h$ and the fast strengthening of the force
outside of that, quickly saturating to a constant. (b) The plot shows the time evolution of the probability distribution of $f_\textrm{z}^\textrm{n}$. Equation \ref{eq-13} is
the explicit formula for generating values of this force. Figure from \autocite{Ferdinandy2012}.
}
\label{fig:csopcsop-landing-1}
\end{figure}

\begin{figure}
\begin{center}
\includegraphics[max width=0.7\textwidth]{chaptercsopcsop/fig4.eps}
\end{center}
\caption[Landing results]{The percentage of landed birds as a function of time. The red curve 
corresponds to the case when coupling between the birds is absent, i.e., $f_\textrm{z}^\textrm{a}=0$, the green one corresponds to the coupled case, while the blue curve is the mean field case, i.e., where the radius of $\mathcal{N}$ is infinity instead of $R$. It is clearly seen that in the presence of coupling, the landing is much sharper {\it viz.} the synchronisation among the birds is much greater. It is also notable that increasing the radius of interaction to infinity does not make the landing process relevantly sharper, it merely decreases the time needed to make the decision to land. Figure from \autocite{Ferdinandy2012}.
}
\label{fig:csopcsop-landing-4}
\end{figure}

The averaging is simply the mean of the vertical velocities over the birds in $\mathcal{N}_i$, i.e. birds closer than a given distance measured in the horizontal plane.
\begin{equation}
f_{\textrm{z},i}^\textrm{a} = \langle v_{\textrm{z}}{\rangle}_{\mathcal{N}_i}
\end{equation}

The repulsive force represents the birds trying to keep a given minimal distance
\begin{equation} 
f_{\textrm{z},i}^\textrm{r}= \sum_{j=1}^{N}f_{\textrm{z},ij}^\textrm{r}
\end{equation}
where
\begin{equation}
f_{\textrm{z},ij}^\textrm{r}=\left\lbrace\begin{array}{ll}
A\left(d-|z_i-z_j|\right) & \textrm{if}\phantom{x}0<|z_i-z_j|<d\\
\phantom{zzzzz}0 & \textrm{otherwise.}
\end{array}\right\rbrace.
\end{equation}

The noise is given by the following formulas:
\begin{equation}
\label{eq-13}
f_{\textrm{z},i}^{\textrm{n}}(t)=\alpha\frac{1-\sqrt{1+4\theta+4\theta^2-8\xi_{\textrm{z},i}(t)\theta}}{2\theta}
\end{equation}
where
\begin{equation}
\theta=\frac{1}{2}\left\{1+\textrm{exp}\left(-\frac{t-t_i}{\tau}\right)\right\}.
\end{equation}
In this each SPP is given a preferred stopping time $t_i$, which slowly biases the noise acting on them toward landing (see \autoref{fig:csopcsop-landing-1}).
The force 
\begin{equation}
f_{\textrm{z},i}^{\textrm{h}}=-\frac{C}{20}\left[1+\textrm{tanh}\left\{\frac{10}{R}\left(|z_i-h|-\frac{\Delta h}{2}\right) \right\}\right]\textrm{sign}(z_i-h),
\end{equation} 
keeps the SPP-s at a given height $h$. The bias, coupled through the averaging force is what creates the possibility of the SPP-s to overcome this force, creating the synchronisation effect. On \autoref{fig:csopcsop-landing-4} we can see the effect of this synchronization, that is the landings of all the SPP-s happen over a much shorter period of time than it would without coupling and almost as fast, though later in time than it would for a mean field model (all SPP-s interacting with all other SPP-s). It is interesting to note, that in this model the inner states ($t_i$-s and $\theta$-s), are not observable variables, only from their influence on actual behaviour allows other SPP-s to infer them.

\subsection{Connection to network and control theory}

Control theory concerns itself with the theory of influencing dynamical systems. If we imagine the flocking of some autonomous artificial agents (e.g. the more and more popular drones), one can see the obvious problem. How to coordinate their movement? How to tell them to go left or right? Control theory is often expressed on networks with certain nodes effecting some other nodes and has extensively studied how the different network topologies influence the behaviours of systems. To apply such thinking to collective motion, we must induce a network topology, for which the most straightforward method is that each agent is a node and we draw edges between them, if the two agents interact. Obviously, this generates a time-dependent network, complicating matters, but nevertheless allows the harnessing of network and control theory in collective motion problems \autocite{Vicsek2012}.

The above approach can be used to prove that consensus can be reached in a flocking scenario. Specifically if given a connected undirected graph (with an $a_{ij}$ adjacency matrix) representing communication between agents of which each has a state $x_i$, the algorithm 
\begin{equation}
\dot{x}_i(t) = \sum_{j\in N_i}a_{ij}(x_j(t) - x_i(t))
\end{equation}
asymptotically converges to an average consensus, i.e. to $x_1 = x_2 = \ldots = x_N$, from any initial condition \autocite{controltheory}. This means that as long as the flock can maintain a connected communication graph, they can reach consensus.


\subsection{Leadership}
\label{subsec:leadership}

In many species, individual recognition is most probably not possible, and collective motion seemingly arises on an egalitarian basis, since from the collective motion perspective, they are interchangeable, although even in these cases, leadership may arise from one or few individuals possessing some information not available to others. In other species, specifically in mammals, individual recognition is possible, and complex hierarchies emerge in their group structures. Although a natural assumption would be to point to the most dominant individual as a leader during collective motion, in many cases the identity and some inner state is responsible for leadership roles or leadership may be distributed among the group, with increased dominance levels giving increased influence during collective motion \autocite{Vicsek2012}.

In ungulates leadership is often attributed to a single individual, yet a recent study \cite{bourjade2015} raises interesting questions about the validity of such a concept, based on observation of two groups of 12 and 6 Przewalski horses. Using different definitions of leadership (moving first, moving in front, or eliciting joining to movement), no individuals that could be consistently classified as a leader were identified. Some limitations to that study are that several types of movements were not measured. In addition movements in the breeding season were also not measured; this was deemed problematic because in the breeding season the stallions directly elicit movements of their harems away from other stallions. Also, due to methodological reasons, only short periods of the day were observed. It has been shown in some cases that in the same group different type of leadership hierarchies might arise in different contexts \cite{Watts150518,nagy2013context}, and there are examples in nature where certain individuals in animal groups consistently act as leaders, for example in zebras and dolphins \cite{larissa2009,fischhoff2007}. Thus, although it is not very clear, how leadership works among these horses, the concept of a single leader can be valid for purposes of modelling.

It should be noted, that the concept of leadership, as we have just seen, is not exact. Leadership could mean starting movement, or leading movement, or for example leading from the back (although a common example of the alpha male wolf leading from the back of the pack is in fact not true \autocite{alphawolf}). Thus when reading the literature about leadership in actual systems attention must be paid to the exact type of leadership implied.



\subsection{Motivation}
\label{sec:introduction}

Living in social structures with multiple levels of hierarchy is widespread in the animal kingdom \cite{Grueter2012a,Grueter2012}. Examples range across several taxa, beginning with humans and primates \cite{Abegglen1984,Kummer1968}, through elephants \cite{Wittemyer2005}, to whales \cite{Baird2000,Whitehead2012} and equids \cite{Feh2001,Rubenstein2004}. There are numerous examples of subgroups  forming around a single individual. For example groups may emerge around a matriarch from her descendants, like in african elephants \cite{Wittemyer2005}, sperm whales \cite{Whitehead2012}, and
killer whales \cite{Baird2000}. Alternatively a reproductive unit may form around a breeding male with several breeding females and their young as in Przewalski
horses \cite{Boyd1994} and plains zebras \cite{Rubenstein2004}. These breeding units can sometimes also include non-breeding males as well, like in hamadryas baboons \cite{Kummer1968} or geladas \cite{Dunbar1975}. 

Our aim with the study presented in this chapter was to examine the way in which such a two-level hierarchy may spontaneously emerge in a group and what implications that hierarchy might have for the collective motion of the group. Our motivation and empirical basis was the collective motion of a Przewalski horse herd in light of group formation within the herd, aided by observations made in \cite{ozogany2014} at the Hortobágy National Park in Hungary. As mentioned before, the Przewalski horse herd is split into harems, organized around a breeding male, with several breeding females and their young offspring. So-called bachelor groups, which consist of males that do not have there own harem are also present \cite{hartmann2012}. It should be noted, that although zebra harems form herds in the wild and have a very similar social structure to the Przewalski horse, the Przewalski herd at Hortobágy is only semi-wild as it lives in a bounded environment, which may force them into a herd. Although this has not been studied thoroughly, park officials reported, that the initial population did not form a herd, which only appeared after the growth of population density.

Both the collective motion of several different species of animals \cite{Vicsek2012}, and the emergence of hierarchy within the social system of the Przewalski horse \cite{ozogany2014} have already been modelled. Conversely, the collective motion of animals that are hierarchically organized into subgroups within a larger group have not been modelled. Thus, we aimed to construct a model of group formation and collective motion of a herd composed of sub-groups as a self-propelled particle model in two dimensions, where we identified leaders forming harems, and followers making up these harems. As we have discussed in \autoref{subsec:leadership} that although attributing leadership to a single individual might not be applicable in all circumstances, it does have explanatory power in a wide range of scenarios. As such it stands to reason that conceptualizing the division between leaders and followers dichotomously helps simplify modelling at a minor cost. Simplifying modelling is helpful in the initial understanding of the type of collective movement of hierarchical herds as it abstractifies much of the ethological complications of a harem. 


Herein we consider an earlier SPP model of collective cell movement introduced in \cref{subsec:szabo2006} \cite{szabo2006} and extend it with a two level hierarchy by introducing two distinct types of particles (i.e.\ leaders  and followers) while simultaneously attempting to limit the increase in the number of parameters. In contrast with \cite{ozogany2014} the group formation is not driven by the environment of the herd, but by interactions dynamically evolving during the collective motion of the individuals. While formulating the model, we concentrated on mimicking the movements of Przewalski horses. While this specificity adds some complexity to our model, relative to what is usual in statistical physics, it is mostly related to nuances in movement and does not play a major factor in group formation, which was our main focus. 

Our study could have potential implications for understanding how and why group formation occurs in nature, how group formation affects the system in which it is happening and the rules governing collective motion in a two-level system. Inferring the universalities and the particulars of the different kind of mechanisms, could potentially be used to artificially control both living and human-made systems, such as domestically kept horse herds or flocks of drones.
