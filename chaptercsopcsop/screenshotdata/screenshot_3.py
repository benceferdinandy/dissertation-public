import numpy as np

data = np.loadtxt("20160301baseline_seed_17.dat")

f1 = 2980
f2 = 3000
print(data[f1*200,0])
print(data[f2*200,0])
a1 = np.mean(data[f1*200:f1*200+200,1:3],axis=0)
a2 = np.mean(data[f2*200:f2*200+200,1:3],axis=0)
print(a1)

q1 =  (np.max(data[f1*200:f1*200+200,1:3] -a1,axis=0))
q2 =  (np.min(data[f1*200:f1*200+200,1:3] -a1,axis=0))
q3 = (np.max(data[f2*200:f2*200+200,1:3]- a2,axis=0))
q4 =  (np.min(data[f2*200:f2*200+200,1:3]- a2,axis=0))

print (q1)
print (q1[0] - q2[0])
diff = np.array([600,0])

div = 100

n1 = (data[f1*200:f1*200+25,1:3] - a1)/div
n2 = (data[f1*200+25:f1*200+200,1:3]-a1)/div
n3 = (data[f2*200:f2*200+25,1:3]-a2 + diff)/div
n4 = (data[f2*200+25:f2*200+200,1:3]-a2 + diff)/div


np.savetxt("screenshot_3_f1m.dat",n1)
np.savetxt("screenshot_3_f1f.dat",n2)
np.savetxt("screenshot_3_f2m.dat",n3)
np.savetxt("screenshot_3_f2f.dat",n4)
