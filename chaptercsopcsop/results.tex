\section{Results}

\label{sec:results}

Our model, with the given parameters, produces a cohesive and ordered motion of the entire herd, while forming groups around leaders and also bachelor groups from group-less leaders. This is in qualitative agreement with the actual observed herd moving on an open plane and as an interesting extra phenomenon, our model also includes ``fights'' between leaders for followers. By ``fights'' we mean a situation where two or more leaders without groups get extremely close to one or more followers and after a short time, one of the leaders ``wins'', i.e. a follower is ascribed to be in the leader's group for long enough for it to chase away the other leaders (see video 1 on the CD supplement and \cref{fig:screenshot3}).  

We found that the forming of groups within the herd causes cohesiveness to drop compared to a case without groups. We also found, that in accordance with but with a greater precision than the previous study, the group size distribution of the horses living in the Hortobágy National Park is lognormal. In contrast to this, the current model, based solely on spatial interactions, gives a normal distribution, which implies that spatial interactions alone are not enough to produce the observed group structure.

\subsection{Cohesiveness of the herd}
\begin{figure}
\centering
\includegraphics[max width=\textwidth]{figure2.pdf}%
\caption[Ordering of the model]{Starting from a uniform random distribution of positions and velocities (left side) the herd forms groups and exhibits ordered motion (right side). Blue dots represent leaders and red dots represent followers (see Supplementary video 2 for a video example).}
\label{fig:screenshot}
\end{figure}

Starting from uniform random initial positions and velocities of the individuals, after sufficient time, the model develops ordered motion throughout the herd while forming groups and thus arriving at a structured and co-moving herd (see \cref{fig:screenshot} and video 2 on the CD supplement). We assumed that during collective migration the horses cannot stop, thus there are two phases of ordered movement:  translational movement, and collective rotation about the -- otherwise slowly moving -- center of mass (see also \cref{fig:screenshot2}). Indeed, when it is not possible to stop (e.g. due to fear), but is not feasible or desirable to move the herd as a whole, herding mammals have been observed to rotate around a common point. To measure translational cohesiveness we use the following translational order parameter

\begin{equation}
\Phi_t = \frac{1}{N} \left |\sum_{i = 1}^{N} \frac{\bm{v}_i}{|\bm{v}_i|} \right |,
\end{equation}

and to measure the rotational cohesiveness we introduce the following rotational order parameter

\begin{equation}
\Phi_c = \frac{1}{N}\sum_{i = 1}^N \mathcal{P}\left(\frac{\bm{v}_i}{|\bm{v}_i|}\right),
\end{equation}

where $\mathcal{P}$ denotes projection onto the normal of the line going through $\bm{r}_i$ and $\bar{\bm{r}}$.

Going from a totally disordered translational movement to totally ordered translational movement $\Phi_t$ will grow from 0 to 1, while $\Phi_c$ will move from -1 to 1, as the system moves from a totally ordered rotation around the center of mass in one direction, through no collective rotation to totally ordered rotation in the other direction.

We found that the system, with parameters given in \cref{tab:parameters} switches between two modes, one of ordered rotation and one of ordered translational motion (see \cref{fig:motiontypes} and video 3 on the CD supplement for an example of a transition from rotational to translational motion). Since the horses in the model do not have the capacity to stop, in an event of indecision about the direction to move they must rotate about a common axis, namely the center of mass. By averaging over the full length of 1000 runs in total, we found that the rotations have no specific direction, as expected (Mann-Whitney U-test, $p=0.96$ on left-right similarity). 


\begin{figure}
\centering
\includegraphics[max width=\textwidth]{figure3.pdf}%
\caption[Switching from translational to rotational ordered movement.]{The herd as a whole either exhibits an ordered translational motion or rotates around a slowly drifting center of mass. These two different types of motions can be distinguished due to the values of the translational (a) and rotational (b) order parameters. The plots are from the same specific run of the model, with the curves smoothed by a window of $\Delta t = 1000$. The spikes during the translational phase are caused by the confining wall (see video 4 on the CD supplement for a sample of an interaction with the wall).}
\label{fig:motiontypes}
\end{figure}

\begin{figure}
\centering
\includegraphics[max width=\textwidth]{screenshot_2.pdf}%
\caption[Rotation and translation with CoM boundary]{A rotational and a translational phase of two specific instances of the simulations with the boundary of the force $\bm{F_\text{com}}$ (see \autoref{eq:kunal}) shown in green. Large blue dots represent leaders and small red dots represent followers. During translation movement the main function of $\bm{F_\text{com}}$ is to keep stragglers within the herd, while during rotation we see that while one part of the herd would go one way, the other part another, and the force keeps them together while the two groups otherwise do not interact too much.}
\label{fig:screenshot2}
\end{figure}

\begin{figure}
\centering
\includegraphics[max width=\textwidth]{screenshot_3_highlight.pdf}%
\caption["Fights" among leaders]{More than one leader without a follower may be drawn to followers. In this case one of them will ultimately succeed in gaining the follower in its "harem", while the losers are "fought" off to a distance. See the green bordered group with three leaders (large blue dots) crowding around some followers (small red dots), where one of them is driven off rather fast. Eventually, one of the remaining two will win. Time progresses from the left side of the image to the right side, in a specific instance of the simulations. }
\label{fig:screenshot3}
\end{figure}

\begin{figure}[ht]
\centering
        \includegraphics[max width=\textwidth]{figure4.pdf}

\caption[Pair-correlation of the herd]{%
The pair-correlation for a herd composed of leaders and followers, but only calculated on the leaders (solid line), and in the case where only leaders are present (striped line). In the latter case the distances are scaled with $R_{LL}^{*EX}/R_{LL}^{EX}$, to compensate for the effect of no leader having a group (leaders without followers can be closer to each other than ones with followers). The main structure of the herd, even with followers, is set by the leaders, but the presence of followers slackens the rigidity of this structure.
}
\label{fig:densitycorr}
\end{figure}

Calculating the pair-correlation function

\begin{equation}
\rho(\bm{r}) = \langle \delta(\bm{r}-\bm{r}_i) \rangle,
\end{equation}
for the leaders in the normal scenario (i.e. where followers are present) and in the scenario where followers are missing, we find that the main structure of the herd is given by the leaders, and introducing followers only slightly loosens this (aside from the fact that it increases the distances between the leaders, see Figure \ref{fig:densitycorr}). We also investigated the effect of introducing followers among the leaders on the order parameter of the translational movement. Comparing $\Phi_t$ (calculated using only the velocities of the leaders in two cases, one where there are only leaders and one where there are also followers) we find that order is decreased when allowing for followers and forming of groups (see Table \ref{tab:orderparam}). This loss in the efficiency of the movement of the herd as a whole points to benefits gained from social groupings outside the paradigm of simple locomotion.

\begin{table}[ht!]
\centering

\begin{tabular}{@{}lllll@{}}

\toprule
&  $\langle\Phi_t\rangle$ & $\langle\Phi_t\rangle$ (only leaders) & $\langle\Phi_c\rangle$ &$\langle \Phi_c\rangle$ (only leaders) \\
\midrule
%short leaders & 0.745 & 0.745 & 0.010 & 0.010  \\
%short all & 0.567 & 0.609 & 0.000 & 0.000 \\
w/o followers & $0.866 \pm 0.016$ & -- & $-0.002 \pm 0.011$ & -- \\
with followers & $0.608 \pm 0.018$ & $0.633 \pm 0.017$& $0.025 \pm 0.020$ & $0.026 \pm 0.021$  \\
%shortmanymales & 0.802 & 0.802 & 0 & 0  \\
\bottomrule

\end{tabular}

\caption[Order parameters of the collective motion model.]{The translational and rotational order parameters averaged over 120 simulations with standard errors. The duration of the runs were many times longer than the stabilization of groups. The first row is from simulations where only leaders were present, the second row is the full model with followers. In this case the averages were calculated on the whole herd as well as on the leaders only. Adding followers and thus moving in groups decreases the order of translational movement, implying that group formation has  benefits other than increased herd cohesion. Although the herd would rotate often, as  expected, there is no specific direction of the rotation (Mann-Whitney U-test on 1000 runs, where the simulations was terminated at a time not long after stabilization of groups yields a $p = 0.96$ on left-rigth similarity).}\label{tab:orderparam}

\end{table}


\subsection{Group size distribution}
Starting from a uniform random spatial distribution and group-less state, the model, after 
sufficient time, will produce co-moving groups based on the relative positions of leaders and followers. The emerging group size distribution is normal, although some leaders and followers may not belong to a group. We define groups by the highest (non-zero) $D_{ij}$ values of the followers, i.e a group consists of the leader and the followers with their highest $D_{ij}$ rating corresponding to this leader. This effectively means that groups are formed by followers spending the most time with a specific leader.  The group size distribution rapidly reaches a close-to-final state and after some time relaxes to the final state (see Figure \ref{fig:haremdistributiontime}). We show the transition by creating a histogram of the group sizes at regular intervals during a simulation and taking the sum of the differences of each respective bin of the histogram in two consecutive measurements, averaged over 1000 independent simulations .

\begin{figure}[ht]
\centering
        \includegraphics[max width=\textwidth]{figure5.pdf}

\caption[Group stability]{%
The group size distribution quickly stabilizes as it is shown by the plot of $\Delta$. To calculate $\Delta$ we create a histogram of the group sizes at regular intervals during a simulation and take the sum of the differences of each respective bin of the histogram in two consecutive measurements. Each point is averaged over 1000 independent simulations. 
}
\label{fig:haremdistributiontime}
\end{figure}
On Figure \ref{fig:haremdistribution} we show a comparison of the simulated distribution with real data obtained from a Przewalski horse herd (see \cite{ozogany2014} for details). Since harem sizes gradually change over time among the horses, the real data has been improved by taking into account historical harem size distributions, showing a more clear lognormal distribution than in the previous study of \cite{ozogany2014}. In this previous study a network model was formulated to account for the lognormal distribution of the group sizes, while the current model, based on purely spatial interactions was not able to reproduce this. This indicates that at this level of complexity, it is not possible to reduce social interactions to spatial interactions.

\begin{figure}[ht]
\centering
        \includegraphics[max width=\textwidth]{figure6.pdf}

\caption[Group size distribution of the model and the real herd]{Comparison of the group size distribution in the model with an empirical one (the group size distribution of Przewalski horses living in the Hortobágy National Park). The empirical distribution follows a clearer lognormal distribution than in \cite{ozogany2014} due to the incorporation of historical data. The distribution obtained from the model is close to normal and is mean-fitted to the empirical distribution. We attribute the difference to the fact that the social interactions of horses are too complex to capture in purely spatial interactions.
}
\label{fig:haremdistribution}

\end{figure}



\subsection{Dimension scales}
\label{sec:dimscales}
Horses usually travel by walking, which is roughly around $\unitfrac[1.5]{km}{h}$ based on our aerial observations averaged over several minutes. In this model $v^0_L$ is the corresponding parameter of the walking speed. To compare our model's length scale with that of reality we have calculated the pair-correlation function of the of the wild horses by using aerial pictures of the real herd and that of the herd in our model and compared the first peaks. This roughly equates the arbitrary length unit of our model to $\unit[0.18]{m}$ in reality (c.f. $R_\text{LF}^{EX}$ in Table  \ref{tab:parameters}). From this we can calculate that the arbitrary time unit of our model is roughly equal to $\unit[0.44]{s}$%0.43636363636363634
. This puts $\tau_\text{L}$ and $\tau_\text{F}$ at about reaction time ($\unit[0.5-1.5]{s}$), $\tau_\text{A}$ to about 3 and a half minutes, and the emergence of a coherent collective motion, with stable harems to slightly less than 10 minutes. Since $\tau_\text{L}$ and $\tau_\text{F}$ both characterize a fast cognitive process it is not unrealistic that the characteristic times are on the scale of reaction times. Since in wild horses the groups do not form from randomly distributed individuals spontaneously, but rather evolve in an already laid down social context, the time needed for group formation is not readily comparable to that of the real herd. On the other hand, for a group of 200 unfamiliar individuals, where leaders are already appointed and everybody is actually already moving, the 10 minutes seems like a reasonable time for group formation (the authors' personal experience with spontaneous group formation in human groups of comparable sizes would allow for even longer times).





