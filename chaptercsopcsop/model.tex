\section{Our model}


\label{sec:model}

The model is based on \cite{szabo2006} in which a model was developed to depict the collective motion of cells. We modified this model to accommodate two types of SPP-s (leaders and followers), asymmetric interactions and group formation rules. While extending this model we aimed at minimizing the number of extra parameters. Compared with the usual SPP models the model of \cite{szabo2006} gives smoother results due to intrinsic relaxation times.  We choose parameters that allow the development of motion that resembles the movements of a herd made of harems as close as possible within the framework of the model. We provide a graphical overview of the model in Figure \ref{fig:modeldescription} and an introduction here.
 

The movements of the horses in the model are confined to a square area, large compared to the size of the herd, representing the herding area available to them (Figure \ref{fig:modeldescription} boundary). Periodic boundary conditions were not considered, first, because it is not realistic, and second, because it does not make sense in a co-moving herd to conceptualize that the front may interact with the rear. Also, we introduce a tendency for horses that stray too far from the herd to head back while still going in the general direction of the herd's (Figure \ref{fig:modeldescription} a)).

All horses may follow all other horses, but the strength of the interaction depends on the types and orientations of the SPP-s in question. Given, that it is plausible that leaders must also pay attention to followers, they will follow followers too, but to a much smaller extent than the other way around. Although the interactions taking place are based on metric distances, we introduce a directedness, meaning that a horse will follow the ones in front of it more than the ones behind it. Several types of interaction modes have been suggested in modelling collective motion. Early models used a simple metric distance, e.g., interacting with anybody nearer than a given distance \cite{vicsek1995}. Later topological distances were introduced, e.g., interacting with a fixed number of nearest neighbours \cite{ballerini2008}. Recently it has been proposed that the most biologically correct interaction ranges should be based on visual perception \cite{strandburg2013}. In our case, vision plays little part as equine vision is near 360° \cite{brooks1999} and neither the distances within the herd nor the density of the herd imply that occlusion would have a major effect on interactions. As such, the effect of following the ones in front, rather than the ones behind is related more to the logic of not turning around if there are others heading in the same direction as oneself.

Leaders who acquire followers (i.e., a harem), will stay farther away from other leaders than if they were without followers (Figure \ref{fig:modeldescription} b) and c)). Harems are established based on spatial distance, but followers will gradually belong more and more to the leader they follow, making it easier for them to stay close, because of the stronger and slightly longer distance interactions with their leader than with another leader (Figure \ref{fig:modeldescription} d)).

Our model starts from randomized initial positions and velocities, without followers being assigned to any leader, thus all followers find groups and leaders at the same time. Our model forgoes the introduction of complex social rules by using only spatial interactions as described above and not taking into consideration that in reality, a new horse would be introduced to a herd already split into harems. On the other hand, taking the latter into consideration would not allow for the study of emergent group formation.


\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figure1.pdf}

\caption[Model overview]{Graphical overview of the model depicting a small herd inside the boundary with  various parts of $\bm{F}_\text{int,r}(\bm{r}_i,\bm{r}_j)$ and $\bm{F}_\text{com}$ shown. Radii are drawn to scale (cf. Table \ref{tab:parameters} for actual values), and the herd is magnified from within the boundary to show the forces. Solid arrows depict direction of forces, dashed arrows depict actual velocities. The following details are included: a) a horse farther from the center of mass than the given boundary (large green circle centred on the center of mass) will move towards the herd but also in the direction the herd is going, b) and c) leaders without groups can go closer to each other than to a leader with a group, while followers can go even closer to a leader, d) the attraction radius of the follower-leader interactions is generally smaller than that of the leader-leader interactions, but it is increased when interacting with the leader of the follower's group.
}%
\label{fig:modeldescription}


\end{figure}
\subsection{Formal model description}
\label{sec:formal model}
We have $N_\text{L}$ number of leaders and $N_\text{F}$ number of followers (the list of parameters can be found in Table \ref{tab:parameters}). The 2-dimensional motion of the horse $i \in \lbrace 1$,$ N = N_\text{L} + N_\text{F} \rbrace$ is described by the overdamped dynamics
\begin{equation}
\diff{\bm{r}_i(t)}{t} = v_i^0\bm{n}_i(\theta_i) + 
\sum\limits_{\substack{j=1\\j\neq i}}^N\bm{F}_{\text{int}}(r_{ij},\varphi_{ij}) 
+ \bm{F}_\text{com}(\bar{\bm{r}}-\bm{r}_i,\bar{\bm{v}}) + 
\bm{F}_\text{wall}(\bm{r}_i,\bm{v}_i) + \boldsymbol\xi
\end{equation}

where $t$ is time, $\bm{r}_i$ is the position of and $\bm{v}_i$ is the velocity of horse $i$, $v^0$ is a preferred speed which differs for leaders and followers, $\bm{n}_i$ is a unit vector characterized by the angle $\theta_i$,  $\bm{F}_{\text{int}}$ is a pairwise interaction with $r_{ij} = |\bm{r}_i - \bm{r}_j|$ and $\varphi_{ij}$ being the angle between $\bm{r}_i - \bm{r}_j$ and $\bm{v}_i$, $\bm{F}_\text{com}$ is a global force dependant on the position ($\bar{\bm{r}}$) and the velocity ($\bar{\bm{v}}$) of the center of mass of the herd, $\bm{F}_\text{wall}$ is the force acting at the boundaries and $\boldsymbol\xi$ is a vector whose components are delta-correlated white noise terms with zero mean.

The direction of the self-propelling velocity $\bm{n}_i(t)$, described by the angle $\theta_i(t)$, attempts to relax to $\bm{v}_i(t) = \operatorname{d}\!\bm{r}_i(t)/ \operatorname{d}\!t$ with a relaxation time $\tau_i$:

\begin{equation}
\frac{\operatorname{d}\!\theta_i(t)}{\operatorname{d}\!t} = \frac{1}{\tau_i}\operatorname{arcsin}\!\left[\left( \bm{n}_i(t) \times \frac{\bm{v}_i(t)}{|\bm{v}_i(t)|}\right)\cdot \bm{e}_z\right],
\end{equation}
where $\bm{e}_z$ is a unit vector orthogonal to the plane of motion, and $\tau_i$ differs for leaders and followers. This relaxation provides smooth transitions of the $\bm{n}_i(t)$ desired velocities. The value of $\tau$ was chosen larger for leaders than followers, implying that leaders are harder to ''convince'' than followers to change directions, but our results are not sensitive to changes in $\tau$. 

The $\bm{F}_{\text{int}}(r_{ij},\varphi_{ij})$ force that carries the direct interaction between the horses can be split into the product of a spatial part ($\bm{F}_{\text{int,r}}(r_{ij})$), and a coefficient part ($F_{{\text{int,}\varphi}}(\varphi_{ij}$), the latter being dependent on the angle of the direction of horse $j$ from horse $i$ and the direction of the velocity of horse $i$. The spatial part consists of a pair-wise, asymmetrical force, the direction of which lies on the line passing through the center of masses of the interacting horses and the magnitude of which is the function of the distance $r_{ij}$ between the horses \cite{szabo2006}. The actual form of the force depends on the type of horses involved:

\begin{equation}
\bm{F}_\text{int,r}(r_{ij}) = 
\begin{cases}
\bm{F}_\text{LL}(r_{ij}), & \mbox{if } i \mbox{ and } j \mbox{ are both leaders,}  \\
\bm{F}_\text{FL}(r_{ij}), & \mbox{if } i \mbox{ is a follower and } j \mbox{ is leader,}\\
\bm{F}_\text{LF}(r_{ij}), &\mbox{if } i \mbox{ is a leader and } j \mbox{ is a follower,} \\
\bm{F}_\text{FF}(r_{ij}),& \mbox{if } i \mbox{ and } j \mbox{ are both followers.} 
\end{cases}
\end{equation}

For all four cases there are two radii defined, $R^\text{AT}$ which is the range of attraction, and a smaller radii $R^\text{EX}$, which is the range of repulsion, and also a distance $L$, which defines a distance inside $R^\text{AT}$ but outside of $R^\text{EX}$, splitting the force into four parts depending on distance, namely a repulsive, an attractive and two non-interacting regimes, with different coefficients for all four types of interaction in both the interacting regimes ($F^\text{AT}$ for the attractive and $F^\text{EX}$ for the repulsive), thus having 8 radii with 8 coefficients and 4 distances. On the example of $\bm{F}_\text{LF}(r_{ij})$ the equations look like this (leader-leader and follower-leader interactions are slightly different):

\begin{equation}
\label{eq:sampleforce}
\bm{F}_\text{LF}(\bm{r}_i,\bm{r}_j) = \bm{e}_{ij} \times
\begin{cases}
F^\text{EX}_\text{LF} \frac{r_{ij}-R^\text{EX}_\text{LF}}{R^\text{EX}_\text{LF}}, & r_{ij} < R^\text{EX}_\text{LF},\\
0, & R^\text{EX}_\text{LF} < r_{ij} < R^\text{EX}_\text{LF} + L_\text{LF}, \\

F^\text{AT}_\text{LF} \frac{r_{ij}-R^\text{EX}_\text{LF}}{R^\text{AT}_\text{LF} - R^\text{EX}_\text{LF}-L_\text{LF}}, & R^{EX}_\text{LF} + L_\text{LF}\le r_{ij} \le R^\text{AT}_\text{LF}, \\

0, & R^\text{AT}_\text{LF} < r_{ij},

\end{cases}
\end{equation}
where $\bm{e}_{ij} = (\bm{r}_i -\bm{r}_j)/r_{ij}$. The non-interacting part between $R^\text{EX}$ and $R^\text{AT}$ was chosen to be very small its only function being is to remove some ''vibrations'' that arise at such low densities, when a horse is on the edge of the attractive and repulsive regimes. The form of the force is one of the simplest ways to define gradually growing forces based on distances and the values of the specific parameters were chosen to imitate that leaders with harems wish to protect their followers from other leaders, while bachelor leaders themselves can create groups.

In the cases of leader-leader ($\bm{F}_\text{LL}$) and follower-leader ($\bm{F}_\text{FL}$) interaction this picture is slightly changed due to the formation of groups. Followers will develop a certain amount of affinity to leaders who are close by, that increases in strength when they are close to the leader and decreases when they are farther away from the leader. Each follower keeps track of time spent near each leader with the quantities $D_{ij} \in [0,\infty]$, which follow the simple dynamics
\begin{equation}
\label{eq:maledist}
\frac{\operatorname{d}\!D_{ij}}{\operatorname{d}\!t} = 
\begin{cases}
+1, & r_{ij} \leq R^\text{AT}_\text{LF}, \\
-1, & r_{ij} > R^\text{AT}_\text{LF} \text{ and } D_{ij} > 0, \\
\quad 0, & r_{ij} > R^\text{AT}_\text{LF} \text{ and } D_{ij} \leq 0.
\end{cases}
\end{equation}
This is then translated into an affinity
\begin{equation}
\label{eq:affinity}
A_{ij} = 2A\left(\frac{1}{1+\exp\left(\frac{-D_{ij}}{\tau_A}\right)}-0.5\right)+1,
\end{equation}
where $\tau_A$ is the characteristic time of affinity increase and $A$ is a constant. The form of \cref{eq:affinity} was chosen so that $A_{ij}$ goes smoothly from $1 \rightarrow A+1$ as $D_{ij}$ goes from $0 \rightarrow\infty $. This effectively changes the parameters in \cref{eq:sampleforce} (but not in  \cref{eq:maledist}!) for the $\bm{F}_\text{FL}$ case from $F^\text{AT}_\text{FL} \rightarrow A_{ij}F^\text{AT}_\text{FL}$ and from $R^\text{AT}_\text{LF} \rightarrow A_{ij}R^\text{AT}_\text{LF}$. This allows a follower to split farther from the leader it belongs to, without leaving the harem, thus introducing more consistency into the group compositions.

The definition of groups is based on the values $D_{ij}$. Every follower is considered to be in the group of the leader for which the value of $D_{ij}$ is largest for the given follower. The leader--leader interaction differs in one aspect if either of the participating leaders have a group, by effectively increasing the repulsive radius $R^\text{EX}_\text{LL}$ of both leaders fivefold when interacting with each other. As such two leaders can be close to each other only if they don’t each have their own groups. This is reminiscent of the distinction between bachelor groups, where males are close together and harems, where the males are farther apart.

The velocity dependent part is the same for both leaders and followers:
\begin{equation}
F_{\text{int,}\varphi}(\varphi_{ij}) = \frac{-1}{1+\exp(-4(\varphi_{ij}-\frac{\pi}{2}))} + 1,
\end{equation}
which effectively means, that a horse will pay more attention to horses that are in front of it, rather than those that are behind it. The form was chosen because of the saturation properties. The total interaction is thus
\begin{equation}
\bm{F}_{\text{int}}(r_{ij},\varphi_{ij}) = F_{\text{int,}\varphi}(\varphi_{ij})\bm{F}_\text{int,r}(r_{ij}).
\end{equation}

The force $\bm{F}_\text{com}$ keeps the herd roughly together, since if one strays farther than $R_\text{com}$ from the center of mass of the herd it will experience the force 
\begin{equation}
\bm{F}_\text{com}(\bar{\bm{r}}-\bm{r}_i,\bar{\bm{v}}) = 
F_\text{com}\frac{|\bm{r}_i - \bar{\bm{r}}|-R_\text{com}}{R_\text{com}}\left( \frac{\bar{\bm{r}}-\bm{r}_i}{|\bar{\bm{r}}-\bm{r}_i|} +
\beta\frac{\bar{\bm{v}}}{|\bar{\bm{v}}|}
\right),
\label{eq:kunal}
\end{equation}
where $\beta$ is parameter that tunes how much the horse is guided in the direction the center of mass is heading and $F_\text{com}$ is the overall strength of the force. Since $R_\text{com}$ is relatively large this force is usually inactive, but will smoothly guide a lost horse back into the herd (adopted from \cite{kunal2010}, see \cref{fig:kunalfig}).


\begin{figure}[ht!]

\centering
        \includegraphics[max width=0.9\textwidth]{kunalborder.jpg}

\caption[Snapshot of the force $\bm{F}_\text{com}$]{Snapshot of the force $\bm{F}_\text{com}$ with $\beta=0.75$ and the velocity of the center of mass of the flock pointing along $Y$ axis. The force guides any stray members of the flock smoothly back with the forces growing proportionally away and back from the center of mass. Figure from \autocite{kunal2010}.
}%
\label{fig:kunalfig}
\end{figure}

The force $\bm{F}_\text{wall}$ sets the boundary conditions. The herd is confined to a square area defined by the length $D$. This box is impenetrable and horses cannot leave it. For the herd to approach this hard boundary in a realistic way, there is a characteristic distance $R_\text{wall}$ where the force $\bm{F}_\text{wall}$ is turned on:
\begin{equation}
\bm{F}_\text{wall}(\bm{r}_i,\bm{v}_i) = \frac{F_\text{wall}}{2}\left( \sin \left[\pi\left( \frac{R_\text{wall}-d_{iw}}{R_\text{wall}}-\frac{1}{2} \right)\right] + 1 \right)
\begin{pmatrix}
\bm{v}_i\cdot\bm{n}_w \\
\bm{v}_i\cdot\bm{t}_w 
\end{pmatrix},
\end{equation}
where $d_{iw}$ is the distance of the horse from the boundary, $\bm{n}_w$ is the normal vector of the boundary and $\bm{t}_w$ is the tangent vector of the boundary, driving the horses smoothly along the wall (adopted from \cite{viragh2014} and \cite{viragh2014corr}).

Initially both leaders and followers are evenly distributed over a square with a linear size of 500, with velocities also randomly distributed.



\subsection{Parameters}
Going, in a na\"{\i}ve way, from the one-type-particle model of \cite{szabo2006} to the two-type-particle model would increase the number of required parameters from 14 to 30 (some parameters are doubled and some are increased fourfold given every possible combination of the particles). By considering that some of these are unnecessary to duplicate (or make four of) our model has 23 parameters. Of these only 7 are relevant in the sense that the formation of meaningful groups is sensitive to their value (parameters that would destroy cohesion even in a one-type-particle model were not taken into account), not considering the size of the herd. A parameter was considered relevant if an increase by twofold or a decrease by half resulted in 0.1\% of followers not being in a group on average (this is less than one per a realization of the model). For a complete list of parameters see Table \ref{tab:parameters}. Parameters were chosen so that cohesive movement occurs and that group formation happens. Except for cases where there was a reason to do otherwise, parameters that could be different for leaders and follower were kept the same. The distances were chosen based on observations, the coefficients of the various forces were chosen so that the phenomenology of the movements resembles that of a real herd. The leaders are slightly faster than followers so that they are able to stay  in front of their harem. It must be noted, that in many cases, leaders in real-life examples may not be at the front of their group, but rather at the side or behind; we elected to use the leading-from-front paradigm for the purpose of simplicity. Other choices pertaining to parameter value selection have been mentioned in the previous section describing the model.

\begin{table}[]
\centering


\begin{tabular}{@{}llll@{}}
\toprule
variable & description & default value & approx. dimensions   \\ \midrule

\multicolumn{4}{c}{relevant variables} \\ \midrule
 $A$ & affinity of followers for leaders & 1.3  & 1.3\\
 $\tau_\text{A}$ & characteristic time of affinity & 500 & \unit[218]{s}\\
  $F_\text{FL}^\text{AT}$ & strength of F-L attraction & 0.03 & \unitfrac[0.0125]{m}{s} \\ 

  
   $R_\text{LL}^\text{AT} $ & radius of L-L attraction & 200 & \unit[36]{m} \\
 $R_\text{LL}^\text{EX} $ & radius of L-L repulsion & 15 & \unit[2.7]{m}\\
 $R_\text{LL}^\text{*EX} , 5 R_\text{LL}^\text{EX}$ & -- & 75 & \unit[13.6]{m}\\
 $F_\text{LL}^\text{AT} , F_\text{LF}^\text{AT}$ & strength of L-L and L-F attraction & 0.01 & \unitfrac[0.0042]{m}{s} \\ 

$N_\text{L}$ & number of leaders & 25 & 25 \\
$N_\text{F}$ & number of followers & 175 & 175 \\

\midrule        
\multicolumn{4}{c}{irrelevant variables} \\ \midrule
  $F_\text{FF}^\text{AT}$ & strength of F-F attraction & 0.0002 & \unitfrac[0.000083]{m}{s} \\ 
   $F_\text{LL}^\text{EX}$&  strength of L-L repulsion  & 2 & \unitfrac[0.83]{m}{s} \\ 
$v_\text{L}^{0}$ & velocity of leaders & 1 & \unitfrac[0.416]{m}{s}\\
$v_\text{F}^{0}$ & velocity of followers & 0.9 & \unitfrac[0.375]{m}{s}\\
$\tau_\text{L}$ & L velocity relaxation time & 3 & \unit[1.31]{s}\\
$\tau_\text{F}$ & F velocity relaxation time & 1 & \unit[0.44]{s}\\
 $\xi$& strength of the noise  & 0.5 & \unitfrac[0.21]{m}{s}  \\
 $L_\text{LL} , L_\text{LF} ,L_\text{FL} ,L_\text{FF}$& non-interaction distances & 1 & \unit[0.18]{m}\\
 $R_\text{LF}^\text{AT} , R_\text{FL}^\text{AT}  , R_\text{FF}^\text{AT}$ & radii of attraction & 50 & \unit[9.1]{m}\\
 $R_\text{LF}^\text{EX}  , R_\text{FL}^\text{EX} , R_\text{FF}^\text{EX}$ & radii of repulsion & 5 & \unit[0.9]{m}\\
$F_\text{LF}^\text{EX}  , F_\text{FL}^\text{EX} , F_\text{FF}^\text{EX}$&  strength of repulsion   & 5 & \unitfrac[2.08]{m}{s} \\  
  $R_\text{com}$ & radius of the cohesion force & 250 & \unit[45.5]{m}\\
 $F_\text{com}$ & strength of the cohesion force & 2.5 &\unitfrac[1]{m}{s} \\
 $\beta$ & cohesion force parameter & 0.01 & 0.01\\
 $F_\text{wall}$ & strength of boundary repulsion & 3 & \unitfrac[1.25]{m}{s}\\
 $R_\text{wall}$ & distance of boundary repulsion & 200 & \unit[36.4]{m}\\
 $D$ & linear size of bounding box & 10000 & \unit[1800]{m}\\
 \bottomrule
\end{tabular}

\caption[Collective motion model parameters]{Table of the parameters of the model grouped according to relevancy in group formation. L and F abbreviate leader and follower respectively. The approximate proper dimensions are based on a comparison with observed horses, see Section \ref{sec:dimscales} for details.}\label{tab:parameters}
\end{table}




