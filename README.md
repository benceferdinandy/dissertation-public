# PhD Dissertation template for ELTE

use `latexmk -lualatex main.tex -f` to build
if something is not working `latexmk -c` will clean up all

The fonts used here can be downloaded from [here](https://fonts.google.com/specimen/Source+Sans+Pro "Source Sans Pro")

Thesis points template is [here](https://bitbucket.org/benceferdinandy/elte-thesispoints-public/src/master/)

# Breakdown of files

- `main.tex` is the "document", this is where everything starts
- `ELTE_dissertation.cls` the dissertation class, loads a lot of packages, defines the titlepage
- `todonotesextra.sty` defines extra `todonotes` commands, check it if you would like to use them
- the folders `chaptercsopcsop` and `chapterhiv` have the content of the Chapter 3 and 4 (these are more-or-less copy pasted
    from the articles that were already published, that is why they are separate and include all text, figures and the bibliography files)
- the folder `fig` has the ELTE logo
- the folder `introduction` has the preface and the first two chapters
- `conclussion.tex` has Chapter 5
- `bibliography.tex` sets up "Publications" and "Bibliography"
- `refs.bib` has all the references that are not in their respective chapter ref.bibs (so pretty much all refs not in Chapters 3 and 4)
- `selfrefs.bib` has the author's own articles (again if needs be) with some slight modifications
- `összefoglaló.pdf`, `summary.pdf` and `Ferdinandy_2017_DOI_adatlap.pdf` were created externally, and are just included as is.

# selfrefs.bib

We need to have all the articles that the author of the dissertation has in this file, regardless of whether it appeared in
the previous bib files.

Also an entry like this

```
@unpublished{abramsensdog2017,
Title = {Novel machine learning algorithms in automated behaviour analysis},
Author = {Ábrám, Dániel and Daróczy, Bálint and Ferdinandy, Bence and Gerencsér, Linda and Csizmadia, Gábor and Benczúr, András and Miklósi, Ádám},
pubstate = {In prep.}
}
```

needs to be modified by adding `AUTHOR+an = {3=corresponding}`, where the 3 indicates the the third author is the writer of the dissertation.

```
@unpublished{abramsensdog2017,
Title = {Novel machine learning algorithms in automated behaviour analysis},
Author = {Ábrám, Dániel and Daróczy, Bálint and Ferdinandy, Bence and Gerencsér, Linda and Csizmadia, Gábor and Benczúr, András and Miklósi, Ádám},
pubstate = {In prep.},
AUTHOR+an = {3=corresponding},%
}
```

# Hungarian support

For Hungarian you will need at least `biblatex` 3.12 and `biber` 2.12.
