%
% Created by Bence Ferdinandy (bence@ferdinandy.com)
%

\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ELTE_dissertation}[2018/04/27 ELTE PhD dissertation class]
\DeclareOption*{\PassOptionsToClass{\CurrentOption}{book}}
\ProcessOptions \relax
\LoadClass[a4paper,18pt,oneside,justified]{book}


\RequirePackage{todonotesextra}

\RequirePackage[a4paper,bindingoffset=1cm,textwidth=345pt]{geometry}

\RequirePackage{import}
\RequirePackage[version=3]{mhchem} %chemistry
\RequirePackage{enumitem} %enumerate nicer
\RequirePackage[final]{pdfpages}
\RequirePackage{caption}
\RequirePackage{quotchap}
\RequirePackage[export]{adjustbox}

\RequirePackage{epigraph}

%design
\RequirePackage{fancyhdr}


\RequirePackage{booktabs}
\RequirePackage{setspace}
\linespread{1.4}
\renewcommand{\arraystretch}{1.4}
%\onehalfspacing
%\RequirePackage[all]{nowidow}

\RequirePackage{subfig}

%nicer squareroot
\RequirePackage{letltxmacro}
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
\setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
\advance\dimen0-0.2\ht0
\setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
{\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2}}


%figures
\RequirePackage[above,below]{placeins}
\RequirePackage{graphicx}
%~ \RequirePackage{caption}
%~ \RequirePackage{subcaption}



%\setmainfont[Ligatures=TeX]{Source Sans Pro}

\RequirePackage[no-math]{fontspec} 

\RequirePackage{amsmath}
\RequirePackage{amssymb}
\RequirePackage{amsfonts} 
\def\bm{\boldsymbol}

\RequirePackage[tight,nice]{units} %for typesetting units of measurements in a nice way (also loads nicefrac)
\RequirePackage{esdiff} %derivatives and partial derivatives in an easy way


%language
\RequirePackage{polyglossia}
\RequirePackage{csquotes}


\sloppy %make latex prefer looser lines than overflows
%bookmarks and references
\RequirePackage[unicode,bookmarks=true,colorlinks=false,hidelinks]{hyperref}
\RequirePackage[capitalize,nameinlink,noabbrev]{cleveref} %much better autoref than autoref, must come after hyperref to work
\RequirePackage{bookmark}
\hypersetup{bookmarksopen={true}}
\hypersetup{bookmarksopenlevel={2}}
\hypersetup{bookmarksnumbered={true}}
%subject
\def\@subject{}
\newcommand{\subject}[1]{
  \def\@subject{#1}
}



\AtBeginDocument{
  \hypersetup{
    pdftitle = {\@title},
    pdfauthor = {\@author}
    pdfsubject
  }

  \hypersetup{
    colorlinks,%
    citecolor=black,%
    filecolor=black,%
    linkcolor=black,%
    urlcolor=black
}
}


%%%%%%%%%%%%%%%%%%%%%
% TITLEPAGE
%%%%%%%%%%%%%%%%%%%%%


\fancypagestyle{titlepage}
{
	\renewcommand{\headrulewidth}{0pt}%
   \fancyhf{}%
   \fancyhead[C]{\Large Doctoral Dissertation}
   \fancyfoot[C]{\LARGE\bfseries\@date}
}


\def\@school{}
\newcommand{\school}[1]{
  \def\@school{#1}
}

\def\@schoolhead{}
\newcommand{\schoolhead}[1]{
  \def\@schoolhead{#1}
}

\def\@program{}
\newcommand{\program}[1]{
  \def\@program{#1}
}

\def\@programhead{}
\newcommand{\programhead}[1]{
  \def\@programhead{#1}
}
\def\@supervisor{}
\newcommand{\supervisor}[1]{
  \def\@supervisor{#1}
}

\def\@supervisortitle{}
\newcommand{\supervisortitle}[1]{
  \def\@supervisortitle{#1}
}

\def\@department{}
\newcommand{\department}[1]{
  \def\@department{#1}
}

\def\@faculty{}
\newcommand{\faculty}[1]{
  \def\@faculty{#1}
}

\def\@university{}
\newcommand{\university}[1]{
  \def\@university{#1}
}

\def\@unilogo{}
\newcommand{\unilogo}[1]{
  \def\@unilogo{#1}
}

\def\@unilogoscale{}
\newcommand{\unilogoscale}[1]{
  \def\@unilogoscale{#1}
}




\newcommand\dissertationtitle{
\begin{titlepage}
{	
%a page with just the title, nothing else
\thispagestyle{empty}
\linespread{1}
	\centering\bfseries\LARGE \@title 
		
	\newpage
}



   \thispagestyle{titlepage}
   \centering
   \vskip2truein
    
   {\bfseries\LARGE \@title }\vskip0.14truein


   \vskip0.25truein
   {\LARGE \@author}\vskip0.5truein
   {\large \@school}
   
   head of school: \@schoolhead
   \vskip0.1truein

	
   { \@program}
   
   head of program:\@programhead

   \vskip0.4truein
   {\includegraphics[scale=\@unilogoscale]{\@unilogo}}
   \vskip0.4truein
   {\Large Supervisor:}\vskip0.05truein
   {\large  \@supervisor \\ \@supervisortitle} \vskip0.15truein

	{\Large   \@department}\vskip0.05truein
	{\Large   \@faculty}\vskip0.05truein
   {\LARGE \scshape \bfseries \@university}\vskip0.1truein
   
\end{titlepage}

}

