This chapter will present our study titled "HIV competition dynamics over sexual networks: first comer advantage conserves founder effects" \autocite{Ferdinandy2015} as an example of agent-based modelling. The study concerns itself with the modelling of different strains of HIV/AIDS infections competing on a realistic human sexual network. In the next section we will overview the background literature needed to understand the different aspects of the problem and then present our specific motivation in creating the model. The rest of the sections of the chapter will present our work on the problem: the agent-based model we created, the results we obtained, and its discussion.


\section{Background literature}
\subsection{Complex networks}
The study of complex system very often involves the study of complex networks. A network is essentially a graph, and network theory is essentially graph theory, albeit sometimes networks are defined to be some subset of graphs. Generally, since the mathematical literature uses the graph and a good number of applications use the network terminology, the latter appears more often when graphs are used to model some real-world phenomena.

\subsubsection{Erdős-Rényi model and real world networks}
When tackling the Königsberg bridge problem Euler studied a small graph, but complex networks cannot be handled in the same way, and have to be treated statistically, which leads naturally to the study of random graphs. The Hungarian mathematicians Alfréd Rényi and the somewhat legendary Pál Erdős proposed the first random graph model in 1959 \autocite{hivintro1}.

In its original formulation, the Erdős-Rényi graph is formed by taking $N$ nodes, and randomly placing $E$ edges between them. A slightly different formulation is placing edges between nodes with a probability $p$ instead of fixing their number. It is clear, that if we choose $p$, so that 
\begin{equation}
pN(N-1)/2 = E
\end{equation}
in the thermodynamic limit of $N \rightarrow \infty$, the two formulations are statistically equivalent. In the first formulation, the number of edges are known exactly, in the second formulations the existence of edges are independent of one-another, making each formulation good in different scenarios. The Erdős-Rényi model was a very important step, but has shortcomings when trying to model real world phenomena. To understand this, first we will introduce some properties of networks. Although there are many more, three properties will be of interest to us: the degree distribution, clustering coefficient and the average shortest path length.

The degree of a node is the number of edges attached to it, thus the degree distribution of a network carries information on how the edges are distributed among the nodes. The clustering coefficient $c_i$ of the node $i$ is defined as the ratio between the number of edges $e_i$ among its nearest neighbours and its maximum possible value $k_i (k_i - 1)/2$, that is 
\begin{equation}
c_i = 2e_i /k_i (k_i - 1).
\end{equation}
Then the clustering coefficient of the network is given by $\langle c_i \rangle$. It gives an indication of how embedded nodes are in a network. The third property we mention is the average shortest path length $\langle l_{ij} \rangle$ in the network, which is a measure of how easy it is to go from one node to the other. The name very much implies the definition: the shortest path length is the minimum number of edges traversed while going from one node to the other.

The degree distribution of the Erdős-Rényi model in the thermodynamic limit is easy to calculate when we take the definition with the probability $p$. On average the graph will have

\begin{equation}
\langle E \rangle = \frac{1}{2}N(N-1)p
\end{equation}
edges, and since each edge contributes to the degree of two nodes, the average degree is

\begin{equation}
\langle k \rangle = \frac{2\langle E \rangle}{N} = (N-1)p \approx Np.
\end{equation}
If $\langle k \rangle < 1$ then the network will be a bunch of small disconnected components, thus usually only the $\langle k \rangle > 1$ regime is investigated.

To arrive at the $P(k)$ degree distribution we notice, that the probability of a node with degree $k$ is equal to the probability that it is connected to $k$ nodes and not connected to the other $N-1-k$ nodes, leading us to the binomial distribution in the form of

\begin{equation}
P(k) = \binom{N-1}{k}p^k(1-p)^{N-1-k},
\end{equation}
which in the thermodynamic limit with $pN = \langle k \rangle$ can be approximated with the Poisson distribution

\begin{equation}
P(k) = e^{-\langle k \rangle} \frac{\langle k \rangle^k}{k!}.
\end{equation}
The clustering coefficient also yield easily from the independent connection probability, since for a given node the probability that two of its neighbours are also connected is given by the probability $p$, thus giving
\begin{equation}
\langle c \rangle = p = \frac{\langle k \rangle}{N}.
\end{equation}

The scaling of the average shortest path length $\langle l \rangle$ can be approximated with the following argument. The number of neighbours within a distance of $d$ of a given node can be approximated with $\langle k \rangle^d$. Since this grows exponentially fast when $\langle k \rangle > 1$, if we choose $d$ to be $\langle l \rangle$ we should have approximately all the nodes within this distance, thus $\langle k \rangle^d \approx N$, leading us to 
\begin{equation}
\langle l \rangle \propto \frac{\log{N}}{\log{\langle k \rangle}}.
\end{equation}

%https://tex.stackexchange.com/questions/16604/easiest-way-to-delete-a-column
\begin{table}[ht]
\centering


\begin{tabular}{@{}lllllll@{}}
\toprule
Network & $N$ & $\langle k \rangle$ & $\langle l \rangle$ &$\langle l \rangle_\text{rand}$ & $\langle c \rangle$ & $\langle c \rangle_\text{rand}$\\ \midrule

WWW, site level, undir. & 153127& 35.21 & 3.1 & 3.35 & 0.1078 & 0.00023 \\
Internet, domain level & 3015--6209 & 3.52–4.11 & 3.7–3.76& 6.36–6.18 & 0.18–0.3 & 0.001\\
Movie actors & 225226 &  61 & 3.65 & 2.99 & 0.79 & 0.00027\\
LANL co-authorship & 52909 & 9.7 & 5.9 & 4.79 & 0.43 & 1.8$\times$10\textsuperscript{-4}\\
MEDLINE co-authorship& 1520251 & 18.1 & 4.6 & 4.91 & 0.066 & 1.1$\times$10\textsuperscript{-5}\\
SPIRES co-authorship & 56627 & 173 & 4.0 & 2.12 & 0.726 & 0.003\\
NCSTRL co-authorship & 11994 & 3.59 & 9.7 & 7.34 & 0.496 & 3$\times$10\textsuperscript{-4}\\
Math. co-authorship &70975 & 3.9 & 9.5 & 8.2 & 0.59 & 5.4$\times$10\textsuperscript{-5}\\
Neurosci. co-authorship & 209293 & 11.5 & 6 & 5.01 & 0.76 & 5.5$\times$10\textsuperscript{-5}\\
\emph{E. coli}, substrate graph& 282 & 7.35 & 2.9 & 3.04 & 0.32 & 0.026\\
\emph{E. coli}, reaction graph & 315 & 28.3 & 2.62 & 1.98 & 0.59 & 0.09\\
Ythan estuary food web & 134 & 8.7 & 2.43 & 2.26 & 0.22 & 0.06\\
Silwood Park food web & 154 & 4.75 & 3.40 & 3.23 & 0.15 & 0.03\\
Words, co-occurrence & 460902 & 70.13 & 2.67 & 3.03 & 0.437 & 0.0001\\
Words, synonyms & 22311 & 13.48 & 4.5 & 3.84 & 0.7 & 0.0006\\
Power grid & 4941 & 2.67 & 18.7 & 12.4 & 0.08 & 0.005\\
\emph{C. elegans} & 282 & 14 & 2.65 & 2.25 & 0.28 & 0.05\\


 \bottomrule
\end{tabular}

\caption[Properties of several real world graphs.]{%
Properties of several read world graphs, adopted from \autocite{albert2002statistical}, the references to the original data source can be found there.

%
} \label{tab:hiv_networkprops}
\end{table}

Numerous real world networks have been measured to calculate the above properties, and although not exclusively, many are very similar regarding these properties. First, in these real networks the degree distributions are heavy-tailed distributions, meaning that their tails are not exponentially bounded (i.e. there is finite chance of finding nodes with a very large degree). Second, they exhibit the so-called small-world property, the famous "six-degrees of separation". Technically, it means that the average shortest path length scales with the logarithm of the number of nodes ($\langle l \rangle \propto \log{N}$) while the clustering coefficient $\langle c \rangle$ is not small \autocite{barrat2008dynamical}. 

The Erdős-Rényi model fails on two accounts with being a model for such real world networks. First, its degree distribution has an exponential cut-off, and second, its clustering coefficient is very small when using the number of edges and nodes from real world examples. \autoref{tab:hiv_networkprops} lists several networks with their measured average degree, average shortest path length and clustering coefficient. The latter two are also calculated if the network were randomized (the same number of edges placed randomly among the same number of nodes, i.e. as an Erdős-Rényi graph). It can be clearly seen that the clustering coefficients are much larger, than what an Erdős-Rényi graph would yield.



\subsubsection{The Watts-Strogatz model for small-world networks}

\begin{figure}[ht]

\centering
        \includegraphics[max width=\textwidth]{figs/wattsstrogatz.png}

\caption[Rewiring in the Watts-Strogatz network model.]{Rewiring in the Watts-Strogatz model. Each node is connected to its four nearest neighbours. As $p$ is increased more and more edges are rewired.  At $p = 1$ all edges have been rewired. Figure from \autocite{wattsstrogatz}.
}%
\label{fig:hiv-wattsrtogatz}
\end{figure}

\begin{figure}[ht]

\centering
        \includegraphics[max width=0.8\textwidth]{figs/wattsstrogatz_clustering.png}

\caption[Small world properties of the Watts-Strogatz model.]{The plot shows that as $p$ is increased from 0 to 1 there is a regime where the clustering $\langle c \rangle $ is still large while the average shortest path length $ \langle l \rangle$ drops rapidly. In this regime the Watts-Strogatz model has small-world properties. Figure from \autocite{internetbook}.
}%
\label{fig:hiv-wattsrtogatz_clusterin}
\end{figure}

To solve the issue of the low clustering coefficient Watts and Strogatz introduced the model named after them, which has a tunable clustering coefficient. The model starts with $N$ nodes in a ring (see \autoref{fig:hiv-wattsrtogatz}), which are connected to the $m$ nearest neighbours on both sides. Then for each node each edge connected to a counterclockwise (right side) neighbour is rewired with a probability $p$ to a randomly chosen other node. It should be noted that even at $p = 1$ the graph will not be totally random, since half the edges are not rewired even in that case. The degree distribution of the Watts-Strogatz model can be obtained analytically \autocite{internetbook} yielding
 
\begin{equation}
P(k) = \sum_{n=0}^{\min{k-m,m}} \binom{m}{n}(1-p)^n p^{m-n}\frac{(pm)^{k-m-n}}{(k-m-n)!}e^{-pm}, \text{for}\, k \geq m.
\end{equation}

As we can see, this is still has an exponential cut-off, so the degree distribution is very similar to that of the Erdős-Rényi, but the clustering coefficient and the average shortest path length behaves very differently. When $p$ is zero, the graph has a very large clustering coefficient, but a small average shortest path length. A small increase in $p$ will not effect the clustering, since only a few edges are rewired, but they introduce significant shortcuts within the graph. If we observe \autoref{fig:hiv-wattsrtogatz_clusterin} we will see that there is a large regime of $p$, where the clustering coefficient is essentially the same as with $p=0$ yet the mean shortest path length is greatly reduced, which regime is more in line with observations of real world networks, although this model still lacks a heavy-tailed degree distribution.

\subsubsection{The Barabási-Albert model}

The most famous and ubiquitous heavy-tailed degree distribution is the $P(k) \propto k^{-y}$ power law distribution, describing the so-called scale-free graphs. The name derives from the homogeneous property of such a degree distribution, that is $P(\lambda k) = \lambda^{-\gamma} P(k)$, which means that any rescaling of the degrees will only offset the distribution by a constant factor. A notable feature of the power-law distribution is that on the $[1,\infty[$ interval, it only has a well defined mean if the exponent $\gamma$ is bigger than two, and a well defined variance if $\gamma > 3$. If one tries to do the actual calculation it should be noted that when talking about power-law degree distribution it is customary to use the continuous $k$ approach, i.e. to treat the degree as a continuous variable and also, that in the continuous approach one must introduce a $k_\text{min}$ minimum degree for the integrals to converge. The exponent of many real world networks fall between 2 and 3, although in reality, a network is called scale-free if the power law holds for 3 orders of magnitude, since there is always an upper boundary: either the system size (no node can have more edges than $N-1$) or some other physical limitation. Thus any reference to a real world network's power-law exponents must be understood to hold between some $k_\text{min}$ and $k_\text{max}$ values.

If scale-free networks are so common in very different scenarios, we start looking for some universal property that explains it. László Barabási and Réka Albert proposed a mechanism, which can yield power-law networks through preferential attachment of links to nodes that already have many links. The model starts with $m_0$ nodes and no edges. Then for each step we 
\begin{enumerate}
\item add a new node, with $m <m_0$ edges
\item connect the other end of the $m$ edges to already present nodes with a probability $k_i/\sum_jk_j$
\end{enumerate}
i.e. the probability of connecting a new edge to an old node is proportional to its degree. Using the continuous $k$ approximation we can show that this mechanism leads to the degree distribution $P(k) \propto k^{-3}$. The derivation goes like this. The probability that the node $i$ acquires a new edge is proportional to it degree as defined by the model
\begin{equation}
P(k_i(t)) = \frac{k_i(t)}{\sum_jk_j(t)}.
\end{equation}
The growth of the degree is governed by the equation 
\begin{equation}
\label{eq:barabasi-2}
\diff{k_i(t)}{t} = m P(k_i(t))
\end{equation}
with the condition that $k_i(i) = m$ as each node is introduced with $m$ edges. Since at each step we add $2m$ to the total number of degrees \autoref{eq:barabasi-2} will take the following form:
\begin{equation}
\diff{k_i(t)}{t} = \frac{mk_i(t)}{2mt},
\end{equation}
which yields
\begin{equation}
k_i(t) = m\left(\frac{t}{i}\right)^{1/2}.
\end{equation}
The degree distribution is 
\begin{equation}
P(k,t) = \frac{1}{t+m_0} \int_0^t\delta(k -k_i(t))di = - \frac{1}{t+m_0}\left.\left( \diff{k_i(t)}{i}\right)^{-1} \right\rvert_{i = i (k,t)},
\end{equation}
where $\delta$ is the Dirac delta function and $N = t+m_0$. This gives 
\begin{equation}
P(k,t) = 2m^2 \frac{t}{t+m_0}k^{-3} \xrightarrow[t \to \infty]{} 2m^2k^{-3}.
\end{equation}
The average shortest path and the clustering coefficient can also be calculated with some approximations, giving short paths but a low clustering coefficient as the system size increases \autocite{barrat2008dynamical}:
\begin{equation}
\langle l \rangle \propto \frac{\log{N}}{\log{\log{N}}},
\end{equation}

\begin{equation}
\langle c \rangle = \frac{m}{8N}(\ln{N}^2)
\end{equation}
\subsubsection{Generalized random graphs}

Although it is good to know that preferential attachment leads to a scale-free behaviour, for modelling purposes we need a tool that can produce power law degree distribution with arbitrary exponents. The method of generalized random graphs is a method to create a graph with a degree distribution of our choice for a given number of nodes. Given $N$ nodes and a $P(k)$ distribution we generate a ${k_i}$ sequence of $N$ numbers from $P(k)$ and assign these as the desired $k_i$ degrees of each node. For each degree of a node, the node receives a half-edge, then these half-edges are paired up randomly. Obviously if $\sum_i k_i$ is odd this cannot be done so care must be taken to only generate sequences with an even sum, although in practice, if $N$ and the number of edges are large enough, we can safely discard the last orphan half-edge in an odd case.
 
We now have a tool to generate networks with power-law degree distributions of given exponents, but is the small-world property present? It can be shown, that the average shortest path lengths scale with the logarithm of the graph size, which is good, but the clustering coefficient goes to zero as $N$ increases, which is less so. Fortunately the situation is not as bad as in case of the Erdős-Rényi graph. The clustering coefficient is very similar to the Erdős-Rényi $\langle c \rangle = \langle k \rangle / N$, but has another factor
\begin{equation}
\langle c \rangle = \frac{\langle k \rangle}{N}\left[ \frac{\langle k^2 \rangle- \langle k \rangle}{\langle k \rangle^2}\right],
\end{equation}

which can be rather large for the graph sizes of observed networks. For instance, Newman showed that just the power law degree distribution accounts for much of the clustering of the World Wide Web \autocite{newman2003structure}.

\subsection{Epidemiology}

\subsubsection{SI, SIS and SIR models with full mixing}

Epidemiology concerns itself among other things, with question about the spreading of diseases in given populations. Classical epidemiological modelling starts from the assumption of "full mixing", which means that all individuals have equal chance of coming into contact with an infected individual (i.e. mean field approach, or in case of networks the Erdős-Rényi model). This assumption allows for easily solvable differential equation, but can be dropped to use actual contact networks, to introduce more realistic models. Based on the full mixing assumption, there are three simple models of infection spreading, named after the allowed states of individuals in the model. There are three such states: susceptible (S), infected (I) and recovered (R), which  are also called compartments, since each individual of the population can be assigned to one of these compartments. A susceptible individual may be infected by an infected individual, becoming an infected individual, while an infected individual may recover from infection and become recovered (R) and immune to further infection. The most basic model is the SI, where only S $\rightarrow$ I can happen (e.g HIV). In the SIS model, an infected individual may become susceptible again (e.g. flu) and in the SIR model an infected individual may become recovered (e.g. mumps). In the following let us investigate the properties of these models based on Chapter 9 of \autocite{hivintro1}.

Let us denote the ratio of susceptible, infected and recovered individuals in the population with $s$, $i$ and $r$. Based on the full mixing assumption each individual is in contact with $\langle k \rangle$ other individuals. Assuming that in a given $\text{d}t$ time an infected individual will infect a susceptible individual with a probability $\beta$, then in the $\beta \text{d}t \ll 1$ limit the probability for a susceptible individual to get infected is $\beta \langle k \rangle i\text{d}t$. Given that the influx of individuals into to infected compartment is proportional to the number of susceptible individuals and $s(t) = 1-i(t)$ in the SI model, we can write the following differential equation to describe the SI dynamics: 
\begin{equation}
\diff{i(t)}{t} = \beta \langle k \rangle i(t) [1 -i(t)].
\label{eq:SIbasic}
\end{equation}
As can be readily seen from the equation, the SI model leads eventually to all individuals being infected, leaving only the speed of the spreading at question.

The SIS model has a bit more variation to it, since in this case an already infected individual may become healthy and susceptible again. Assuming an infected individual becomes susceptible again with the probability $\mu\text{d}t$, we can modify the previous equation like thus:
\begin{equation}
\diff{i(t)}{t} = -\mu i(t) + \beta \langle k \rangle i(t) [1 -i(t)].
\label{eq:SISbasic}
\end{equation}

The SIR model complicates this with a third state. In this case the  $\mu\text{d}t$ still denotes the probability of leaving the susceptible state, but now to the recovered and not the susceptible state, further complicating the equations:
\begin{equation}
\diff{s(t)}{t} = - \beta \langle k \rangle i(t) [1 - r(t)-i(t)],
\label{eq:SIR1basic}
\end{equation}
\begin{equation}
\diff{i(t)}{t} = -\mu i(t)+\beta \langle k \rangle i(t) [1 - r(t)-i(t)],
\label{eq:SIR2basic}
\end{equation}
\begin{equation}
\diff{r(t)}{t} = \mu i(t).
\label{eq:SIR3basic}
\end{equation}

Note, that both the SIS and the SIR model behave the same when taking the limits of the $\mu/beta$ ratio. By taking the limit of $\mu \gg \beta$ infected individuals heal faster than they can infect other individuals, leading to the epidemic dying out quickly. In contrast, taking the $\mu \ll \beta$ limit all terms containing $\mu$ are negligible leading effectively to the SI model. Next we will explore the behaviour of these models.

The initial condition of an epidemic is a small fraction of infected individuals, i.e.  $i \ll 1$ so we need only keep the terms that are first order in $i$
\begin{equation}
\diff{i(t)}{t} = \beta \langle k \rangle i(t)
\label{eq:SIlin1}
\end{equation}
leading to the solution 
\begin{equation}
i(t) \simeq i_0 e^{\beta \langle k \rangle t},
\label{eq:SIlin2}
\end{equation}
for small $t$-s, where $i_0$ is the initial ratio of infected individuals. The solution defines the $\tau = (\beta \langle k \rangle)^{-1}$ timescale of the spreading of the epidemic. For the SIS and the SIR models taking the terms that are first-order in $i$ leads to the same equation in both cases:
\begin{equation}
\diff{i(t)}{t} = -\mu i(t) +\beta \langle k \rangle i(t).
\label{eq:SIRlin1}
\end{equation}
Similarly as with  \ref{eq:SIlin1} this leads to an exponential solution,
\begin{equation}
i(t) \simeq i_0 e^{t/\tau},
\label{eq:SIRlin2}
\end{equation}
albeit with a different timescale:

\begin{equation}
\tau^{-1} = \beta \langle k \rangle -\mu.
\label{eq:SIRlin3}
\end{equation}

The main difference here is that $\tau$ in the SIS and SIR models may be negative, leading to the withering of the epidemic. This allows for the definition of the so-called epidemic threshold as such:
\begin{equation}
\tau^{-1} = \mu (R_0 -1) > 0,
\label{eq:SIRlin4}
\end{equation}
where $R_0 = \beta \langle k \rangle  / \mu$ is the basic reproductive rate, which has to be larger than 1 for an epidemic to occur.

\subsubsection{Infection spreading in graphs}

The above equations can be modified to take into account heterogeneity in the underlying contact network. This can become very important when modelling real-life networks and as we will later see, human sexual networks are very heterogeneous. Previously we took the average number of contacts and assumed each individual has that many contacts. Let us relax this assumption and allow each individual to interact with as many individuals as s/he has contact with. To do this we will introduce the quantities $s_k$, $i_k$ and $r_k$ as the ratio of individuals who are susceptible, infected or recovered respectively, among the individuals with $k$ number of contacts. Assuming the only difference between individuals is their number of contacts and introducing $\Theta_k$, the density of infected individual in the neighbourhood of individuals with $k$ contacts, we arrive at the following SI equation:
\begin{equation}
\diff{i_k(t)}{t} = \beta[1-i_k(t)]k\Theta_k(t).
\label{eq:SIRhet1}
\end{equation}
The SIS and SIR models can likewise be adapted. Without detailing calculations, in this case the timescale previously defined changes to the following:
\begin{equation}
\tau = \frac{\langle k \rangle}{\beta \langle k^2 \rangle - (\mu+\beta)\langle k \rangle},
\label{eq:SIRhet2}
\end{equation}
and setting the condition for the spreading of the epidemic (the epidemic threshold) to

\begin{equation}
\frac{\beta}{\mu} \leq \frac{\langle k \rangle}{ \langle k^2 \rangle -\langle k \rangle}.
\label{eq:SIRhet3}
\end{equation}


Note, that in case of $\langle k^2 \rangle / \langle k \rangle\rightarrow \infty$, the epidemic threshold is zero, meaning any kind of infection will spread eventually. As we will see later, human sexual contact networks are modelled with networks where this is the case in the thermodynamic limit.



\subsection{Introduction to HIV and AIDS}
\subsubsection{History of HIV}
The acronym AIDS stands for Acquired Immune Deficiency Syndrome and is a very recent addition to the various diseases plaguing humanity, which attacks the immune system, and if left untreated, leads to death through secondary infection which would not be able to attack a healthy individual. First diagnosed in the western world in 1981 in the United States of America, initially it was regarded as a disease only affecting homosexuals, since it first started  amongst homosexual men. Later it was diagnosed in intravenous drug users and an infant died to due AIDS, rapidly leading to larger awareness. After it also spread to Europe international efforts began to stop it. In 1983 the cause of AIDS was found: Human Immunodeficiency Virus (HIV).



The origin of HIV can be traced back to Central Africa. Monkeys and apes in Africa have been infected with the Simian Immunodeficiency Virus (SIV) for at least 30 millennia before the start of the HIV pandemic, possibly passing over to humans several times, but these strains of HIV presumable died out. There are several types of HIV, but the global pandemic of  infections is being driven mainly by the group M lineage of HIV-1, which crossed the species barrier from chimpanzees to humans about 100 years ago, most possible in Léopoldville (today Kinshasa, Democratic Republic of Congo) \cite{hiv1,hiv2}. By the time it started to spread beyond its epicentre in Central Africa, the virus had already accumulated considerable sequence diversity \cite{hiv2}, and distinct divergent clades initiated a series of rapid expansions that gave rise to the subtypes of HIV-1 group M \cite{hiv3,hiv4}. 

There are several theories on why over the last 30000 years HIV could only start a pandemic in the 20\textsuperscript{th} century, but they all share a common theme of tying it to the colonization of Africa: rapidly growing cities, destabilized social structures and widespread infections of sexually transmitted diseases. One theory is that the organised medical fight against the sexually transmitted diseases was the direct cause of the spreading of HIV. At that time Léopoldville had 2-4 times as many men as women residents and a considerable part of the women were involved in so-called soft prostitution with several men, leading to a high level of syphilis. The countermeasures to syphilis was the regular vaccination of the population with needles of questionable sterility due to poor economic conditions, spreading HIV very quickly.

On \cref{fig:hivintrofig1,fig:hivintrofig2}  we can observe 20 years of the pandemic from 1990 to 2010, showing that although it is still a major issue, the number of new infections and the number of deaths related to HIV is on the decline, thanks to prevention and treatment  efforts, but as we shall see later, our results suggest that the future may hold some unpleasant surprises.

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/hivprevalence.pdf}

\caption[The global prevalence of HIV]{The global prevalence of HIV from 1990 to 2010. Figure taken from \autocite{hivintro2}.
}%
\label{fig:hivintrofig1}
\end{figure}

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/hivdeath.pdf}

\caption[New infections and deaths by HIV]{The yearly number of new HIV infection and deaths related to HIV. Both the number of new infections and deaths are on a decline. Figure taken from \autocite{hivintro2}. 
}%
\label{fig:hivintrofig2}
\end{figure}

\subsubsection{Infection and disease}
HIV can be transmitted via blood, semen, vaginal secretions and breast milk and attacks a type of immune cell called the CD4\textsuperscript{+} T lymphocytes. It seems that most infections are started by a single virion. In the initial phase of the infection, the acute phase, HIV starts to rapidly multiply and mutate (the latter being one of the major causes that we still do not have a vaccine). During the acute phase the virus concentration is extremely high, while the virulence is also high and the diseased may suffer from various symptoms.

The acute phase lasts a few months then passes into the chronic phase. In the chronic phase there are no symptoms, the virulence is low, and this may last for years. At the end of the chronic phase the number of T cells drops to such a low level, that AIDS develops. AIDS itself usually does not cause death in itself, but the lack of a normal immune system makes diseases deadly, that a healthy individual would not even notice \autocite{pathogenezis1}.

\subsection{Motivation}
\label{sec:hivintroduction}
The global molecular diversity of the pandemic still bears the clear footprint of the strong founder effects that characterized this initial expansion. While diversity is very high near the epicentre of the epidemic in Central Africa, the epidemics of other regions are typically characterized by the dominance of at most a few subtypes or circulating recombinant forms (CRFs) \cite{hiv5}, see \autoref{fig:hivintrofig3}. The countries where more than one subtype is prevalent tend to be characterized by parallel, compartmentalized epidemics with distinct subtypes infecting different risk or ethnic groups \cite{hiv6,hiv7,hiv8,hiv9}, and transmission chains rarely cross national borders \cite{hiv10}. While the global spatial distribution of HIV subtypes is not completely static, the diversification of the epidemic and shifts between subtypes occur very slowly in most regions \cite{hiv5}.
Understanding the factors that set the time scale of HIV competition dynamics at the population level has great practical relevance. Subtypes differ in both transmissibility \cite{hiv11,hiv12,hiv13} and the rate of disease progression \cite{hiv14,hiv15}, and further variation in these traits is bound to exist within the subtypes and in the vast diversity of unique recombinant forms (URFs) and unclassified basal lineages in Central Africa \cite{hiv16,hiv17,hiv18}. Virus variants that have higher transmission potential are likely to be spreading at the expense of less efficient strains, and epidemics may expand as the original variants are gradually replaced by "fitter" viral lineages. The risk and pace of these processes needs to be better characterized.
We developed a simple model of sexually transmitted HIV epidemics that allowed us to monitor the competition dynamics of distinct virus strains with varying rates of transmission. In sexually transmitted epidemics, HIV is transmitted over the network of sexual contacts, which tends to include a very limited subset of all possible contacts, i.e. the host population is very far from "free mixing". Our aim was to create an agent-based model of the sexual dynamics of a population and model the spreading of the virus over the emerging network. Since in Sub-Saharan Africa the primary way of infection is via heterosexual contact, we based the model on data from generalized heterosexual epidemics in Africa.

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/subtypeprevalence.png}

\caption[Worldwide prevalence of HIV-1 group M subtypes and CRF]{Worldwide prevalence of HIV-1 group M subtypes and CRF. Note, that usually very few subtypes are in one area, except for Africa. Figure taken from \autocite{hivintro3}. 
}%
\label{fig:hivintrofig3}
\end{figure}