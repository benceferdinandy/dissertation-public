\section{Results}
\label{sec:hivresults}
\subsection{Network properties with single strain infection}


\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/disszfig1.pdf}

\caption[Contact degree distribution of males]{{\bfseries Males: }(a) Frequency distribution of the annual number of sexual contacts (realized contact degree) of males in uninfected populations (purple dots) and in populations with high-prevalence epidemics (green squares), based on median data from 1000 simulation runs. Highly promiscuous individuals were selectively depleted in the presence of the virus. (b) Boxplot of the exponents of power-law distributions fitted to male individuals in batches of 1000 independent runs with no virus, low and high prevalence epidemics, respectively. Boxes depict interquartile range, median is indicated by horizontal lines within the boxes, and whiskers extend to the farthest values that are not more than 1.5 times the box width away from the box. Medians (and IQR) of the exponents were 2.59 (2.56–2.62), 2.70 (2.67–2.75) and 2.85 (2.81–2.90) in the absence of the virus and with low or high prevalence epidemics, respectively; all pairwise comparisons between the three scenarios were statistically significant ($p<10^{-10}$; Wilcoxon rank sum test). Simulation parameters were set as in  \autoref{tab:hivtable1}.
}%
\label{fig:hivdisszfig1}
\end{figure}

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/figS3.pdf}

\caption[Contact degree distribution of females]{{\bfseries Females: }(a) Frequency distribution of the annual number of sexual contacts (realized contact degree) of females in uninfected populations (purple dots) and in populations with high-prevalence epidemics (green squares), based on median data from 1000 simulation runs. Highly promiscuous individuals were selectively depleted in the presence of the virus. (b) Boxplot of the exponents of power-law distributions fitted to female individuals in batches of 1000 independent runs with no virus, low and high prevalence epidemics, respectively. Boxes depict interquartile range, median is indicated by horizontal lines within the boxes, and whiskers extend to the farthest values that are not more than 1.5 times the box width away from the box. Medians (and IQR) of the exponents were 3.73 (3.86 - 3.97), 3.83 (3.99 - 4.09) and 3.99 (4.19 - 4.32) in the absence of the virus and with low or high prevalence epidemics, respectively; all pairwise comparisons between the three scenarios were statistically significant ($p<10^{-10}$; Wilcoxon rank sum test). Simulation parameters were set as in Table \ref{tab:hivtable1}.
}%
\label{fig:hivfigS3}
\end{figure}

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/disszfig2.pdf}

\caption[Effects of infection on population.]{(a) The relative contribution of acute stage transmissions over the time course of single-strain epidemics. The proportion of transmissions originating from acute-stage transmitters decreases from high levels at the beginning of the epidemics to a steady-state around 0.15 and 0.13 in the low
(purple dots) and high (green dots) prevalence epidemics, respectively, over a time scale of a few
decades. Proportion data were calculated by combining transmission events recorded in 1000
simulation runs, then smoothed by averaging with a sliding window of 100 weeks length. (b) The ratio of infection among men (purple dots) and (non-FSW) women (red squares) increased with preferred contact degree (number of partners per year; plotted on logarithmic scale). The plot was created from 1000 independent simulation runs of single-strain epidemics of high prevalence, using logarithmic binning, right-censored at the top 1\% of the male/female population (where rare classes result in strong stochastic variation). (c) The probability of infection as a function of the promiscuity (preferred contact degree) of
the individuals: data and model fit. Using collated data from 100 simulation runs (2 million individuals
total), we performed a logistic regression on the probability of infection in individuals using log
transformed preferred contact degree, age and gender as explanatory variables. Purple and red lines
show smoothed actual proportions of infected among females and males, respectively, calculated
with a sliding window (moving along all individuals sorted according to contact degree; each point
representing the frequency of infections among 1000 individuals). Predictions from the logistic
regression (plotted as orange and green lines, using the same sliding window smoothing) provide an
excellent fit to the data. Effect sizes (and 95\% CI) for the three factors were estimated as follows:
$\log_{10}$(degree): 2.48 (95\% CI: 2.46–2.50), age: 0.0460 per year (95\% CI: 0.0457–0.0464), female
gender: 0.420 (95\% CI: 0.413–0.428); all three effects were significant at $p<10^{-10}$. Simulation parameters were set as in \autoref{tab:hivtable1}
}%
\label{fig:hivdisszfig2}
\end{figure}
%Our model tracks a dynamic network of contacts between three types of nodes (individuals): male, female, and female sex worker (FSW). Stochastic processes are implemented for the formation and dissolution of links (heterosexual relationships); sexual acts over the links; transmission of HIV in serodiscordant acts; and death due to AIDS. Natural (non-AIDS related) turnover is implemented by tracking the age of individuals, and replacing individuals at the end of sexual activity; the network of contacts is parameterized based on contemporary data including variability in the promiscuity of individuals. The transmissibility of HIV is dependent on disease stage.

Before exploring the competition dynamics of our model we investigated how the system behaves without a virus, or with a single strain of virus.

First we validated the annual contact degree distribution of the resulting network for both males and females (see \cref{fig:hivdisszfig1,fig:hivfigS3}). Power-law exponents of the realized annual contact degrees (based on the actual numbers of sexual contacts in the last year) were fitted as described in \cite{hiv69}, estimating the lower cutoff with Kolmogorov-Smirnov statistics, using the implementation of \cite{hiv70}. The realized exponent of the annual contact degrees are slightly larger than the empirical, but are still very close.


One of the hypothesis on designing the basic scenario was that an individual will only have one acute phase, even in case of a superinfection. The importance of this is shown on \autoref{fig:hivdisszfig2} A, where the relative contribution of acute stage transmissions in our simulations is depicted: after little over a decade, 80\% of new infections originate from individuals in the chronic phase, lowering the overall ability to infect and thus hindering any subsequent infection from spreading.

Another factor that we hypothesised to negatively effect the spreading of any invader strain, is that the resident strain will deplete the highly connected hubs by selectively killing the highly promiscuous individuals. Indeed, in our simulations the probability of infection was strongly related to the promiscuity (preferred contact degree) of the individuals (\cref{fig:hivdisszfig2} b). Using collated data from 100 simulation runs, logistic regression against log transformed contact degree (controlling also for age and gender) estimated an effect size of 2.48 (95\% CI: 2.46 -- 2.50; $p<10^{-10}$), implying that the odds of being infected increased by a factor of $\exp(2.48)$, i.e., about 12-fold for every order of magnitude increase in the preferred contact degree (\cref{fig:hivdisszfig2} c); the effect was robust also in regressions on individual simulation runs (effect size range in 100 simulations: 2.26–2.66; $p<10^{-10}$ for all simulation runs). As a result, an established epidemic of the resident virus strain depleted highly connected nodes of the network preferentially: the power-law exponent of the contact degree distributions (fitted to the actual number of yearly partners) increased significantly compared with the pre-epidemic steady state ($p<10^{-10}$, Wilcoxon rank sum test; \cref{fig:hivdisszfig1,fig:hivfigS3}), which may also have put any invader strain at a disadvantage.

\subsection{Default scenario}

We simulated a simple scenario of competition between two strains of the virus. To assess the maximum potential for a “first comer advantage”, we started the simulations with one of the strains (the founder, or “resident” strain) and let the epidemics attain steady-state prevalence before introducing the second (“invader”) virus strain. The transmission rate of the invader strain was equal to or greater than that of the resident strain, and its chance and pace of growth was assessed in relation to its transmission advantage over the resident strain.

We hypothesized that the effect on the spread of the invader strain may depend on the prevalence of the resident strain, and have therefore considered two scenarios, where the steady-state prevalence of the resident strain was around 0.03 and 0.2, respectively. The two scenarios were set by changing the baseline rate of transmissibility (see \cref{tab:hivtable1}); all other parameters were kept constant. \autoref{fig:hivfig1} shows the time course of multiple simulations for two selected cases where the invader virus had equal (\autoref{fig:hivfig1} a,c) or 25\% greater (\autoref{fig:hivfig1} b,d) transmission rate compared with the resident strain in the high (\autoref{fig:hivfig1} a-b) or low (\autoref{fig:hivfig1} c-d) prevalence scenarios. The resident strain attains steady-state prevalence in about 84 and 74 years in the low- and high-prevalence scenario, respectively. With equal transmission rate, the invader strain shows no appreciable growth in a hundred years in the high-prevalence scenario (\autoref{fig:hivfig1} a), and grows, but remains in strong minority over the same time span in the majority of the simulations with the low-prevalence scenario (\autoref{fig:hivfig1} c). A 25\% advantage in the transmission rate allowed the invader virus to outgrow the resident strain in both scenarios (\autoref{fig:hivfig1} b,d), but it still took a median of 60 and 104 years until the prevalence of the invader strain reached that of the resident strain in the low- and high-prevalence setting; due to its higher transmissibility, the invader strain was then able to attain higher steady-state prevalence compared with the initial steady state of the resident strain. Compared with the initial expansion of the resident strain, the expansion of the invader was much slower in all cases. In addition, 66.4 and 68.3 percent of the simulations with equal transmissibility of the invader resulted in the extinction of the invader virus in the low- and high-prevalence scenarios, respectively; extinction occurred in 2.4 and 1.2\% of the cases when the invader had 25\% transmission advantage. In contrast, with our settings the initial introduction of the resident virus was nearly always (in 998/1000 and 1000/1000 independent simulation runs of the low and high-prevalence settings, respectively) able to establish an epidemic that grew to steady state.

The timescales seen on \autoref{fig:hivfig1} are quite compatible with the know history of the HIV/AIDS pandemic. In our model it takes decades for the epidemic to stabilize in the population and any second strain also needs quite a few decades to become more prevalent, than the resident strain, which timescales are both congruent with the current epidemic.

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/fig1.pdf}

\caption[Prevalence of resident and invader strain in various scenarios.]{The invader virus had equal (a, c) or 25\% greater (b, d) transmission rate compared with the resident strain in the high (a, b) or low (c, d) prevalence scenarios. The resident strain (solid purple line) was introduced in the population at Week 1000 (to allow the network to attain steady state); the invader strain (dashed green line) was introduced in the population when the first strain had already reached steady-state prevalence (at Week 5000 and 7000 for the high- and low-prevalence setting, respectively). Even with a 25\% advantage in the transmission rate, it took the invader strain a median of 60 and 104 years to reach the prevalence of the resident strain in the low- and high-prevalence scenario, respectively. The lines show median prevalence from simulations where the invader strain did not go extinct (out of 1000 simulation runs); shading indicates the areas between the 5\% and 95\% quantiles. Simulation parameters were set as in \ref{tab:hivtable1}.
}%
\label{fig:hivfig1}
\end{figure}


Our strategy was thus to construct a default simulation scenario using settings that we deemed most plausible (partially inhibited superinfection, with strain replacement when superinfection is successful; one-time acute peak of infectiousness; and emergent preferential depletion of highly connected individuals), then test the effect of switching off one mechanism at a time in a series of test scenarios: i) “dual infection” with possible co-existence of the two strains in the same individual and no inhibition of superinfection; ii) “multiple acute” with repeated episodes of enhanced acute-stage infectiousness upon each successful superinfection; and iii) “fixed degrees” in which the degree distribution of the contact network was preserved. This strategy allowed us to assess the relative impact of each mechanism on the population level competition dynamics, and served also as a sensitivity analysis for relaxing the assumptions of inhibited superinfection and one-time acute peak infectiousness.


\subsection{Inhibition of superinfection dominates first comer advantage}

We tested eight scenarios (default and three test cases, each in both low and high prevalence settings) with levels of relative transmission advantage for the invader strain ranging between 0--50\%. The invader strain was introduced in the population when the resident virus had attained steady-state prevalence; all combinations of scenario and transmission advantage were tested in 1000 simulation runs. We extracted several statistics to quantify the probability and rate of the expansion of the invader virus (\autoref{fig:hivfig3}).

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/fig3.pdf}

\caption[Comparison of competition scenarios]{All quantifiers are plotted against the relative transmission rate advantage of the second (invader) strain, with alternative scenarios to test interference mechanisms. Rows show results from the high (top row) and low prevalence (bottom row) settings; columns depict three different quantifiers; scenarios are coded by symbols and colour. In the default scenario (purple lines and dots) the invader strain faced a high risk of extinction (a, d) and experienced very slow growth to 1\% absolute prevalence (b, e) and to 50\% relative prevalence (c, f) at low values of transmission rate advantage, compared with the initial growth of the resident virus (dashed gray lines). The effect was largely abrogated with unhindered superinfection and co-existence (dual infection scenario; orange lines and diamonds), and, in the high-prevalence setting, partially mitigated by allowing for repeated “acute stage” peak infectivity after superinfection (multiple acute scenario; green lines and triangles); fixing the degree distribution of the contact network (fixed degrees scenario; red lines and squares) had little effect compared with the default scenario. Increasing the relative transmission rate advantage of the invader strain also decreased the inhibition effects: values comparable to the single-strain baseline were observed around 25\%--50\% transmission advantage. Data in b-c and e-f depict medians from 1000 simulation runs (excluding those where the invader virus went extinct). Parameters are listed in \autoref{tab:hivtable1}; scenarios are described in detail in the main text. The maximum length of simulations was 19,000 weeks (~365 years); empty symbols indicate where the invader strain did not reach the threshold prevalence by the end of the simulation in the majority of the cases.
}%
\label{fig:hivfig3}
\end{figure}


When the transmission advantage of the invader strain was small, most simulations of the default scenario resulted in the extinction of the invader variant in both the high (\autoref{fig:hivfig3} a) and the low (\autoref{fig:hivfig3} d) prevalence settings. In contrast, the first (resident) strain was able to establish a stable epidemic in nearly all (>99\%; dashed gray line) simulation runs when introduced into a fully susceptible population, which indicates a strong first comer advantage at the early stages of the spread of new strains. Preserving the degree distribution of the contact network (“fixed degrees”) had negligible effect compared with the default scenario; allowing multiple peaks of acute-stage infectiousness substantially reduced the probability of extinction in the high, but not in the low-prevalence setting. Finally, allowing for unhindered superinfection and coexistence (“dual infection”) reduced the probability of extinction to near zero even with no transmission advantage, illustrating that the inhibition of superinfection was the major factor in the heightened extinction risk of the invader strain. Greater relative advantage in the transmission rate reduced the risk of extinction in all scenarios, approaching zero extinction risk at around 25\% advantage.

We defined two more quantifiers based on the time it took the invader strain to grow to selected threshold levels (in both cases we derived the statistics from the simulation runs where the invader strain did not go extinct). The time to one percent absolute prevalence (infecting one percent of the total population) was selected as a low threshold that would allow for the detection of a new strain in a population (\autoref{fig:hivfig3} b,e). As a baseline comparison, we plotted also the median time until the resident strain attained one percent prevalence during its initial expansion (median of 14.4 and 3.3 years for the low and the high prevalence case; dashed gray lines). At small values of the transmission advantage, growing even to one percent prevalence can take a century or more in the default scenario (e.g. a median of 114 and 228 years with a transmission advantage of one percent, in the low and high-prevalence setting, respectively). The inhibition effect was stronger in the high-prevalence setting, and was gradually lost when the transmission advantage of the invader strain was increased to about 50\%. The dominant mechanism of inhibition was again the inhibition of superinfection: allowing for dual infection abrogated most of the effect even at low values of the transmission advantage. The other two mechanisms of interference had negligible effect in the low-prevalence scenario (\autoref{fig:hivfig3} e), but had some partial effect in the high-prevalence scenario (\autoref{fig:hivfig3} b); multiple peaks of acute infectiousness had a stronger impact than fixed contact degrees also in this test case.

Finally, we also collected statistics on the time until the turning point when the invader strain accounted for 50\% of the infections in the population (\autoref{fig:hivfig3} c,f). This time was extremely long (>300 years) when the invader strain had low transmission advantage in the default scenario, and a transmission advantage of 50 percent was needed to bring it down to a median of 27 and 48 years in the low and high-prevalence setting, respectively (in comparison, the resident strain reached 50\% of its steady-state prevalence in a median of 35 and 40 years in the low and high-prevalence cases; dashed gray lines). Allowing for dual infection again had the strongest impact at lower transmission advantage, followed by allowing for multiple peaks of acute-stage infectiousness.

To understand why the impact of repeated acute-stage infectivity depended on the initial prevalence of the resident strain, we calculated the contribution of superinfection events and acute-stage transmissions to the spread of the invader strain in the various scenarios (\autoref{fig:hivfig4}). As expected, the contribution of superinfection was very low (<5\%) in the low-prevalence setting, where most individuals were uninfected at the introduction of the invader strain; in contrast, many more transmissions (~20\% initially) involved superinfection of carriers of the resident virus in the high-prevalence setting (\autoref{fig:hivfig4} a). Because multiple acute peaks of infectiousness take effect only when superinfection occurs, their impact on the frequency of acute transmissions was much stronger in the high-prevalence setting (\autoref{fig:hivfig4} b--c), and the increased frequency of efficient acute transmissions explains the reduced risk of extinction and faster growth of the invader strain when multiple acute peaks of infectivity were allowed in the high-prevalence scenario. In the high-prevalence setting (\autoref{fig:hivfig4} a), the relative contribution of superinfection decreases faster in the multiple acute scenario compared with the default scenario: the reason for this difference is that multiple acute peaks of infectiousness can substantially accelerate the outgrowth of the invader strain in the high-prevalence scenario (see \autoref{fig:hivfigS5}), and the decline of the resident strain results in a decreasing probability that the invader (super)infects an individual who carries the resident strain.

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/fig4.pdf}

\caption[The contribution of superinfection events and acute-stage transmissions to the spread of the invader strain.]{The contribution of superinfection events and acute-stage transmissions to the spread of the invader strain.
(a) depicts the time course of the proportion of transmissions of the invader strain that involved superinfection of carriers of the resident virus. Coloured lines show smoothed proportion data for low and high prevalence epidemics using the default scenario, and the “multiple acute” scenario that allowed for repeated peaks of acute-stage infectiousness upon superinfection. In both scenarios, the contribution of superinfection was very low in the low-prevalence setting (green and orange lines), where most individuals were uninfected at the introduction of the invader strain; in contrast, many more transmissions involved superinfection in the high-prevalence setting (purple and red lines). (b) depicts the time course of the proportion of transmissions of the invader strain that originated from acute-stage transmitters in the four cases (colour coding is the same in a and b). (c) shows the difference in the proportion of acute-stage transmissions between the default and the multiple acute scenario for both prevalence settings (i.e. the distance between the red and purple, and between the green and yellow lines of Panel b). Allowing for multiple acute peaks of infectiousness greatly increased the proportion of acute-stage transmissions in the high-prevalence setting (purple line), but to a much lesser extent in the low-prevalence setting (green line). In all cases, time courses are plotted from the introduction of the invader strain into steady-state epidemics of the resident strain. Proportion data were calculated by combining transmission events recorded in 1000 simulation runs, then smoothed by averaging with a sliding window of length 100 weeks. Parameters were set as in \autoref{tab:hivtable1}; the transmission advantage of the invader strain was 5\% in all cases.
}%
\label{fig:hivfig4}
\end{figure}

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/v2figS5.pdf}

\caption[The effect of multiple acute infections on the competition of HIV strains]{The effect of multiple acute infections on the competition of HIV strains. The figure
compares the outgrowth of an invader virus with 5\% transmission rate advantage in the high (top
row) and low (bottom row) prevalence settings with default superinfection dynamics (left: a, c) or
with repeated peaks of acute-stage infectiousness upon superinfection (right: b, d). The resident
strain (solid purple line) was introduced in the population at Week 1000 (to allow the network to
attain steady state); the invader strain (dashed green line) was introduced in the population when
the first strain had already attained steady-state prevalence (at Week 5000 and 7000 for the high-
and low-prevalence setting, respectively). Multiple acute peaks accelerate the outgrowth of the
invader strain and the decline of the resident considerably in the high prevalence scenario (A vs. B),
but not in the low prevalence scenario (c vs. d), where superinfection is rare. The lines show median
prevalence from simulations where the invader strain did not go extinct (out of 1000 simulation
runs); shading indicates the areas between the 5\% and 95\% quantiles. Simulation parameters were
set as in \autoref{tab:hivtable1}; scenarios are described in detail in the main text.
}%
\label{fig:hivfigS5}
\end{figure}

\subsection{Short head start or fast population turnover reduce first comer advantage}

We next investigated what happens if the invader strain enters the population when the first strain is still in its growth phase and has not yet reached steady-state prevalence. We ran simulations where the invader was introduced when the resident strain had attained 5\%, 20\% or 50\% of its plateau prevalence level and compared the outcome to the previous default setting (\autoref{fig:hivfig5}). As expected the first comer advantage was weaker when the second strain was introduced early in the growth phase of the first strain. However, the probability of extinction of the invader strain increased substantially already when the resident strain was at only 5\% of its plateau level initially in the low-prevalence setting (\ref{fig:hivfig5} d), or at 20\% of plateau level in the high-prevalence setting (\autoref{fig:hivfig5} a). The time to 50\% relative prevalence of the invader strain was strongly affected when the resident strain was initially at 5\% of its plateau level in the high-prevalence setting (\autoref{fig:hivfig5} c), and at 20\% of plateau level in the low-prevalence setting (\autoref{fig:hivfig5} f). We thus conclude that (depending on the prevalence setting) some aspects of the first comer advantage are established relatively early in the initial expansion of the first successful strain.


\begin{figure}[ht!]

\centering
  \includegraphics[max width=\textwidth]{figs/fig5.pdf}

\caption[First comer advantage in growth phase.]{Quantifiers of the “first comer advantage” when the invader virus enters in the growth phase of the resident strain.
Plotted are cases (coded by symbols and colour) where the invader was introduced when the resident strain had attained 5\%, 20\% or 50\% of its plateau prevalence; in the default case the second virus was introduced at Week 7000/5000 in the low/high prevalence setting (as in \autoref{fig:hivfig3}) when the resident strain had already reached a stable plateau in its prevalence. All quantifiers are plotted against the relative transmission rate advantage of the second (invader) strain. Rows show results from the high (top row) and low prevalence (bottom row) settings; columns depict three different quantifiers. First comer advantage is weaker when the invader enters at earlier stages of the growth of the initial strain. Dashed gray lines in a-b and d-e represent the growth of the resident virus without competition; with early introduction of the invader strain, 50\% relative prevalence in C and F is attained well below plateau prevalence and therefore cannot be compared to the 50\% point of single-virus epidemics as a baseline. Data in b-c and e-f depict medians from 1000 simulation runs (excluding those where the invader virus went extinct). Parameters are listed in \autoref{tab:hivtable1}; competition dynamics followed the default scenario in all cases. The maximum length of simulations was 19,000 weeks (~365 years); empty symbols indicate where the invader strain did not reach the threshold prevalence by the end of the simulation in the majority of the cases.
}%
\label{fig:hivfig5}
\end{figure}

\begin{figure}[ht!]

\centering
        \includegraphics[max width=\textwidth]{figs/v2figS6.pdf}

\caption[The effect of population turnover]{The effect of population turnover on the “first comer advantage”. All quantifiers are
plotted against the relative transmission rate advantage of the second (invader) strain, for two levels
of population turnover: 35 (default, purple dots) or 20 years (red squares) of uninfected (sexually
active) lifespan, in the high (top row) and low (bottom row) prevalence settings. Faster turnover had
little effect on the probability of extinction of the invader strain, but could have a pronounced effect
on its rate of growth at low values of the transmission advantage. Data in b-c and e-f depict medians
from 1000 simulation runs (excluding those where the invader virus went extinct). Parameters are
listed in \autoref{tab:hivtable1}; superinfection and replacement dynamics followed the default scenario. The
maximum length of simulations was 19,000 weeks (~365 years); empty symbols indicate where the
invader strain did not reach the threshold prevalence by the end of the simulation in the majority of
the cases.
}%
\label{fig:hivfigS6}
\end{figure}

We also tested the effect of faster population turnover using a residence time of 20 years (as opposed to the default of 35 years) for uninfected individuals in the population. This scenario may apply to regions that experience intense population movements and/or high rates of non-AIDS mortality. Faster population turnover had little effect on the initial risk of extinction for the invader strain, but could substantially accelerate the rate of its growth in the simulation runs where it did not go extinct (\autoref{fig:hivfigS6}). The probability of extinction is influenced by the instantaneous availability of susceptible individuals, which is not affected by the rate of turnover (at a fixed population size); however, subsequent growth depends on the continuous supply of new susceptibles, which increases with the rate of population turnover.

\subsection{Case study: The expansion of HIV-1 subtype A in Uganda}

While the mechanisms of interference can slow down the invasion of new strains, the global pandemic is not static and major shifts between HIV lineages have been occurring in selected regions. The best-characterized example is the expansion of HIV-1 subtype A at the expense of subtype D in Eastern Africa \cite{hiv5,hiv25,hiv26}, and we used the detailed data from Uganda \cite{hiv26} to derive a crude estimate for the transmission advantage required for the observed expansion. Between 1994 and 2002, the estimated prevalence of subtype D decreased from 11.9\% to 8.1\%, and the prevalence of subtype A increased from 2.8\% to 3.0\% in Uganda; the overall prevalence of HIV declined from 17\% to 13\% over the same period \cite{hiv26}. The overall decline probably reflects changes in risk behaviour and/or health interventions; with stable (17\%) prevalence, the relative expansion of subtype A would roughly correspond to growing to 3.9\% (3$\cdot$17/13) absolute prevalence, over a background prevalence comparable to that of our high-prevalence setting. Analyzing data from our high-prevalence default scenario, we found the closest match with the data when the transmission advantage of the invader strain was set to 25\% (\cref{fig:hivfig1} b), in which case the increase from 2.8\% to 3.9\% prevalence took 7.8 years on average (vs. 8 years in the empirical dataset). The rate of the relative expansion of subtype A observed in Uganda would thus require about 25\% advantage over the resident subtype D strain, in a setting of stable overall prevalence in our simulations. Decreasing overall prevalence in the empirical data indicates a slowing turnover of infections, which requires a greater transmission advantage for the same tempo of strain replacement. This is roughly consistent with the independent empirical estimation that the overall (unadjusted) transmission rate of subtype A was 47\% higher than that of subtype D in the same cohort \cite{hiv12}.

