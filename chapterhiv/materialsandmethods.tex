\section{Our model}

\label{sec:hivmaterialsandmethods}
\subsection{Basic model}
\label{subsec:hivsimulationmodel}
The basis of the agent-based model is the network of sexual contacts, which consists of three types of nodes (individuals): males, (non sex worker) females and female sex workers (FSW) and the links represent sexual contact. The model tracks the age and HIV status (stage of infection and the infecting virus type) of each individual, and for males and (non-FSW) females also a fixed quantifier of promiscuity (a preferred annual contact degree), and the number of distinct sexual partners in the last year (realized annual contact degree). Individuals enter the population at age 15 and are removed at the age of 50, to simulate dropping out of sexual activity. The preferred contact degree of each individual is drawn from an empirical distribution according to the type of the node and is kept constant for the lifetime of the individual. The promiscuity of males and (non-FSW) females are characterized by continuous power-law distributions of the form $P(x) \propto x^{-\gamma}$ (with different exponents for the two sexes, see \autoref{tab:hivtable1}) parametrized based on empirical data and censored at both a lower cut-off (one contact per year to ensure all nodes are active in the network) and an upper cut-off (this is a case where the upper limit given by the network size is unrealistic once it reaches a few thousand, since there is a physical limit for the human body). FSW have a fixed maximum number of one-time contacts per week, representing the businesslike organisation of their sex-life.

The simulations have a time step of one week, and each step consists of the following procedures:
\begin{enumerate}[label=(\alph*)]
\item generation of sexual acts along the links and virus transmission,
\item update of HIV status,
\item birth and death dynamics of individuals,
\item dissolution and formation of network links.
\end{enumerate}

As the simplest assumption, the number of sexual acts in male-female links is drawn from a Poisson distribution (discarding zeros: no links were inactive); male-FSW links always involve a single sexual act. The probability of virus transmission to uninfected individuals is determined by the baseline transmission rate of the virus strain, amplified if the transmitting individual was in the acute stage of the infection. Newly infected individuals are immediately assigned a time to death from a uniform distribution between 3 -- 20 years (consistent with recently estimated survival curves in cohorts not receiving antiretroviral treatment  \autocite{hiv65}), and for each infection event the following are recorded: the date of the event, the strain that was transmitted, the disease stage of the transmitter, and whether the transmission involved superinfection of an individual previously infected with the other virus type. For simplicity, the size of the population is kept constant (at 10,000 individuals of both sexes): all nodes who die of AIDS or leave the network at age 50 (whichever came first) are replaced with a new individual of age 15. The preferred annual contact degree of new nodes is drawn from the power-law distribution of the respective gender at entry to the population. The links between males and females are allowed to form and break up at each time step. The baseline probability of break-up is set to yield an average duration consistent with empirical estimates (see \autoref{tab:hivtable1}), and is scaled proportional to the average contact degree of the two nodes (such that more promiscuous individuals have shorter relationships \autocite{hiv66}).

Link formation is implemented such that all non-FSW individuals will have a yearly number of sexual contacts approximately equivalent to their preferred annual contact degree, using a generalized random graph model. Although a generalized random graph model would allow for generating an exactly matching distribution, in our case only an aggregate (yearly) degree distribution was known, so we had to generate some links at each time step to add up to the preferred yearly contact degree. To do this, at each time step, the nodes are assigned a number of half-links generated randomly in proportion to their preferred contact degree, which was approximated by the preferred yearly contact degree divided by the number of weeks in a year (52). As we will see later, this method slightly increased the realized exponents, partially due to not accounting for links surviving longer than a year.

Because males have greater mean promiscuity than non-FSW females, the number of half-links for males exceed those of the females. New links are formed by first randomly connecting all female half-links to male half-links, then randomly distributing the remaining male half-links to the FSW. All simulation runs of the model start with an initialization phase restricted to link formation and break-up until the sexual network settles to a steady state. FSWs have fixed promiscuity and are added one by one as long as there is a surplus of male half-links. The number of FSWs at steady-state is thus not pre-determined, but emerges to match and compensate the imbalance of male and (non-FSW) female links in each scenario.


Population level competition is simulated by implementing two virus types that are allowed to differ in their rate of transmissibility. The type of the infecting virus strain(s) is tracked for each infected individual. The first virus strain is introduced in a random sample of ten percent of all FSW after the initialization of the sexual network: this method allowed a reliable establishment of the "resident" epidemic with negligible risk of extinction. The second (invader) strain is also introduced in a sample of ten percent of all FSW (sampled from uninfected FSW) when the resident strain has attained a steady state in the population.




\subsection{Mechanisms of interference}

This simulation framework allowed us to investigate three potential mechanisms of interference between the resident and the invader strains. To do this, we defined four scenarios, a default scenario and three other scenarios that differs from the default one in a specific detail. In the following, we will introduce the default scenario and then present the supporting and counter arguments, which will also define our three other scenarios.

In the default scenario, superinfection can occur only by the replacement of the original strain with the superinfecting strain. In a sexual act between two individuals infected with different virus strains, both strains have a chance to be transmitted. Superinfection occurs if two check points are passed: initial transmission occurs according to the transmission rate of the infecting strain (modified by disease stage, if appropriate); then after successful initial transmission, the probability of superinfection is determined by the relative transmission rates ("fitness") of the two strains as follows: $P = \nu_2 / (\nu_1 + \nu_2)$, where $\nu_1$ denotes the transmission rate of the virus infecting the potential recipient and $\nu_2$ denotes the transmission rate of the strain infecting the potential transmitter. This means that at equal transmission rates its a 50-50 game. The "clock" of disease stage in the recipient is unaffected by superinfection in the default scenario; the stage of disease remains to be based on the age of infection defined by the date of the original first infection of the recipient. A new time to death is drawn randomly (from the 3 -- 20 years range); however, it is used only if the new date of death precedes the original date determined at the initial infection: superinfection could never extend the lifespan of an individual. Now that the default scenario is presented, let us investigate it in detail.

First, infection with one HIV strain may afford some protection against superinfection with another strain: both the depletion of target cells and the induction of anti-HIV immune responses are likely to create less favourable conditions for infection compared with an uninfected individual. Because the strength of such an effect is still subject to debate (see \autoref{sec:hivdiscussion}), we used a conservative approach in the default scenario: if two infected individuals with different strains have a sexual act, both strains had a chance to be transmitted in a two-step procedure. The first step tested successful initial transmission, which had a probability based on the transmission rate of the given strain, equivalent to the first infection of an uninfected individual. Then in the second step the superinfecting virus replaced the original strain with a probability based on the relative transmission rates of the two strains. Thus in the default scenario, the first strain had neither advantage nor disadvantage at the within-host level, and the “inhibition effect” arose only from the assumption that the infection of each individual is dominated by a single virus strain, implying replacement rather than coexistence upon superinfection (which is a reasonable simplification for the modelling of population level spread; see \cref{sec:hivdiscussion}). The default scenario model reduces the average probability of superinfection to 50\% of that of initial infection, which is consistent with a recent prospective cohort study that estimated about two-fold lower hazard of superinfection compared with initial infection \cite{hiv19}. However, we also tested a “dual infection” scenario, in which superinfection was completely unhindered, and both strains were able to co-exist within one individual after superinfection occurred. In this scenario, in sexual acts between a dual infected and an uninfected individual both virus strains had an independent probability of being transmitted.

Second, we hypothesized that the peak of infectivity that characterizes acute infection \cite{hiv20,hiv21} may not occur again upon superinfection due to depleted target cell levels and the presence of anti-HIV immune responses. If this is indeed the case then the first virus strain to colonize a population may take advantage of a rapid early wave of expansion fuelled by a high relative frequency of efficient acute stage transmissions in a largely susceptible population. In contrast, any subsequent “invader” strain is limited to the lower rates of chronic transmission that characterize mature epidemics \cite{hiv22}, and even successfully superinfected individuals represent a diminished resource if acute peak infectivity cannot be repeated. We implemented this possible mechanism by keeping track of disease stage independently of the identity of the infecting strain. If superinfection occurred after the end of acute infection, the individual was assumed to remain in chronic stage and the onward transmission of the superinfecting strain occurred according to its baseline (chronic) transmission rate. When superinfection occurred during the acute stage of the initial infection, then the superinfecting strain received the benefit of enhanced acute-stage transmission for the remaining time of the acute stage, timed from the initial infection of the individual. However, some evidence indicates that superinfection can generate a new temporary peak of viremia (when the virus can be found in the blood and is thus easily spreading elsewhere) at least in some of the cases \cite{hiv23}. We have therefore also tested a scenario where superinfection started a new window of enhanced acute-stage infectivity.

Third, we hypothesized that in the absence of broadly available antiretroviral treatment (ART), the first HIV epidemics may also have an impact by selectively infecting and killing highly promiscuous individuals who form the “hubs” of the network. Such individuals have been shown to be particularly important for the spread of sexually transmitted diseases \cite{hiv24}, and they are likely to be infected preferentially due to their larger number of contacts and thus are also more likely to die of AIDS. To assess the strength of this effect, we also implemented a scenario in which all individuals who died of AIDS were replaced by an uninfected individual with the same promiscuity (preferred contact degree) as that of the deceased individual, which preserved the degree distribution of the contact network irrespective of the epidemics.

The parameters of the sexual network are based on contemporary surveys in Africa; HIV parameters are also based on available empirical data (\autoref{tab:hivtable1}). The high-prevalence setting was implemented by increasing (doubling) the baseline transmission rate, consistent with the recent finding that variation in prevalence among Sub-Saharan countries can largely be explained by differences in the rate of transmission in serodiscordant couples, i.e. in couples where only one member of the couple is infected \autocite{hiv67}.

\begin{table}[]
\centering


\begin{tabular}{@{}lll@{}}
\toprule
symbol & description & value [reference] \\ \midrule


$N_{m}$	&Number of men in the population&	10000 \\
$N_{f}$	&Number of women in the population&	10000\\
$\gamma_{m}$	&Exponent of male degree distribution	&2.45\textsuperscript{a} \cite{hiv71}\\
$\gamma_{f}$	&Exponent of female degree distribution&	3.45\textsuperscript{a} \cite{hiv71}\\
$K_{c}$	&Number of clients per FSW per year	& 400\textsuperscript{b} \cite{hiv72,hiv73,hiv74}\\
$\kappa_\text{min}$	&Lower cutoff of annual degree distribution&	1\\
$\kappa_\text{max}$	&Upper cutoff of annual degree distribution	&1000 \textsuperscript{c}\\
$p_{b}$	&Probability of link breakup per week	0.05\textsuperscript{d} & [73]\\
$\lambda$	&Poisson parameter for the number of sex acts per week	&2 \cite{hiv75,hiv76}\\
$\nu_1$	&Strain 1 per-contact transmissibility in chronic stage	&0.001 or 0.002\textsuperscript{e} \cite{hiv77}\\
$\nu_2$	&Strain 2 per-contact transmissibility in chronic stage&	(1–1.5)$\nu_{1}$\\
$m_{A}$	&Transmission multiplier for acute infection	&9 \cite{hiv36}\\
$T_\text{acute}$	&Length of acute phase (weeks)	&12 \cite{hiv20}\\
$T_\text{age}$	&Duration of sexual activity (years)&	35 (age 15-50)\\
$T_\text{HIV}$	&Survival with HIV infection (range in years)&	3–20 \cite{hiv78,hiv79}\\
$T_\text{init}$	&Time steps (weeks) without virus	&1000\\
$T_\text{ single}$&	Time steps (weeks) with only one virus	&6000 or 4000\textsuperscript{e}\\

 \bottomrule
\end{tabular}
\captionsetup{singlelinecheck=off}
\caption[Parameters used for the simulation of the HIV infection model.]{Parameters used for the simulation of the HIV infection model.%
\begin{itemize}
\item[\textsuperscript{a}]Used to generate preferred annual contact degrees; for the exponents fitted to realized contact degrees, see \cref{fig:hivdisszfig1,fig:hivfigS3}.
\item[\textsuperscript{b}]Middle value from 600 given in \autocite{hiv72} and 150 calculated from \autocite{hiv73,hiv74}.%
\item[\textsuperscript{c}]The maximum realized contact degree was lower in all simulations.%
\item[\textsuperscript{d}]Baseline rate for links between individuals with degree 1.%
\item[\textsuperscript{e}]Alternative values used to parameterize low- and high-prevalence settings.%
\end{itemize}
} 
\label{tab:hivtable1}
\end{table}




